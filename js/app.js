var ROIAnalysis = /** @class */ (function () {
    function ROIAnalysis() {
        //Dashboard
        this.employeeNumber = 100;
        this.avgPayroll = 12022; //currency
        this.interventionCost = 700; //currency
        this.renewalServicesCost = 300; //currency
        this.includeProd = 'Yes';
        this.includeLongImpact = 'No';
        this.includeWeightImpact = 'No';
        //Assumptions
        this.defaultAvgPayroll = 50000; //currency
        this.avgAbsenceRate = 5.8 / 230; //percentage
        this.presentMultiplier = 300 / 100; //percentage
        this.medicalCost = 972; //currency
        this.medParticipation = 95 / 100; //percentage
        this.disabilityCostPercent = 0.8 / 100; //percentage
        this.disabilityParticipation = 90 / 100; //percentage
        this.deathCostPercent = 0.4 / 100; //percentage
        this.turnoverRate = 9.5 / 100; //percentage
        this.turnoverCost = 30 / 100; //percentage
        this.productivityMultiplier = 50 / 100; //percentage
        this.IRRforROI = 15 / 100; //percentage
        this.medicalInflation = 8 / 100; //percentage
        this.wageInflation = 2 / 100; //percentage
        //impactSummary
        this.impactSummaryAbsence = 6.440683714 / 100; //percentage
        this.impactSummaryPresence = 3.839433258 / 100; //percentage
        this.impactSummaryMedical = 7.839229865 / 100; //percentage
        this.impactSummaryDisable = 8.497367014 / 100; //percentage
        this.impactSummaryDeath = 9.743985867 / 100; //percentage
        this.impactSummaryTurnover = 3.652593311 / 100; //percentage
        this.impactSummaryProductivity = -4.19659943 / 100; //percentage
        this.residualImpact = 100 / 100; //percentage
        //npv coefficients
        this.npvCoeffYear0 = 0.5;
        this.npvCoeffYear1 = 1.5;
        this.npvCoeffYear2 = 2.5;
        this.npvCoeffYear3 = 3.5;
        this.npvCoeffYear4 = 4.5;
        this.npvCoeffYear5 = 5.5;
        this.npvCoeffYear6 = 6.5;
        this.npvCoeffYear7 = 7.5;
        this.npvCoeffYear8 = 8.5;
        this.npvCoeffYear9 = 9.5;
        this.npvCoeffYear10 = 10.5;
        this.npvSavings0 = 0;
        this.npvSavings1 = 0;
        this.npvSavings2 = 0;
        this.npvSavings3 = 0;
        this.npvSavings4 = 0;
        this.npvSavings5 = 0;
        this.npvSavings6 = 0;
        this.npvSavings7 = 0;
        this.npvSavings8 = 0;
        this.npvSavings9 = 0;
        this.npvSavings10 = 0;
        this.btn = document.getElementById('confirm');
        this.employeesInput = document.getElementById('employees');
        this.payrollInput = document.getElementById('payroll');
        this.interventionCostInput = document.getElementById('interventionCost');
        this.maintenanceCostInput = document.getElementById('maintenanceCost');
        this.includeProductivityInput = document.getElementById('includeProductivity');
        this.btn.disabled = true;
        this.calculate();
    }
    ROIAnalysis.prototype.calculate = function () {
        this.total0 = 0;
        this.total1 = 0;
        this.total2 = 0;
        this.total3 = 0;
        this.total4 = 0;
        this.total5 = 0;
        this.total6 = 0;
        this.total7 = 0;
        this.total8 = 0;
        this.total9 = 0;
        this.total10 = 0;
        this.averageSavings0 = 0;
        this.averageSavings1 = 0;
        this.averageSavings2 = 0;
        this.averageSavings3 = 0;
        this.averageSavings4 = 0;
        this.averageSavings5 = 0;
        this.averageSavings6 = 0;
        this.averageSavings7 = 0;
        this.averageSavings8 = 0;
        this.averageSavings9 = 0;
        this.averageSavings10 = 0;
        this.npvSavings0 = 0;
        this.npvSavings1 = 0;
        this.npvSavings2 = 0;
        this.npvSavings3 = 0;
        this.npvSavings4 = 0;
        this.npvSavings5 = 0;
        this.npvSavings6 = 0;
        this.npvSavings7 = 0;
        this.npvSavings8 = 0;
        this.npvSavings9 = 0;
        this.npvSavings10 = 0;
        var absenceCashFlow = [];
        var presenceCashFlow = [];
        var medicalCashFlow = [];
        var disableCashFlow = [];
        var deathCashFlow = [];
        var turnoverCashFlow = [];
        var productivityCashFlow = [];
        this.avgPayroll ? this.payroll = this.avgPayroll : this.payroll = this.defaultAvgPayroll;
        this.startAbsence = this.payroll * this.avgAbsenceRate * this.employeeNumber;
        this.sixMonthAbsence = this.startAbsence * (1 - this.impactSummaryAbsence);
        this.startPresence = this.startAbsence * this.presentMultiplier;
        this.sixMonthPresence = this.startPresence * (1 - this.impactSummaryPresence);
        this.startMedBenefit = this.medicalCost * this.employeeNumber * this.medParticipation;
        this.sixMonthMedBenefit = this.startMedBenefit * (1 - this.impactSummaryMedical);
        this.startDisableBenefit = this.payroll * this.employeeNumber * this.disabilityParticipation * this.disabilityCostPercent;
        this.sixMonthDisableBenefit = this.startDisableBenefit * (1 - this.impactSummaryDisable);
        this.startDeathBenefit = this.deathCostPercent * this.payroll * this.employeeNumber;
        this.sixMonthDeathBenefit = this.startDeathBenefit * (1 - this.impactSummaryDeath);
        this.startTurnover = this.payroll * this.turnoverCost * this.employeeNumber * this.turnoverRate;
        this.sixMonthTurnover = this.startTurnover * (1 - this.impactSummaryTurnover);
        this.includeProd === "Yes" ? this.startProductivity = this.payroll * this.productivityMultiplier * this.employeeNumber : this.startProductivity = 0;
        this.sixMonthProductivity = this.startProductivity * (1 - this.impactSummaryProductivity);
        var savingsAnnualAbsence0 = this.startAbsence - this.sixMonthAbsence;
        absenceCashFlow.push(savingsAnnualAbsence0);
        var savingsAverageAbsence0 = this.savingAverage(savingsAnnualAbsence0);
        var savingsAnnualAbsence1 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 1);
        absenceCashFlow.push(savingsAnnualAbsence1);
        var savingsAverageAbsence1 = this.savingAverage(savingsAnnualAbsence1);
        var savingsAnnualAbsence2 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 2);
        absenceCashFlow.push(savingsAnnualAbsence2);
        var savingsAverageAbsence2 = this.savingAverage(savingsAnnualAbsence2);
        var savingsAnnualAbsence3 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 3);
        absenceCashFlow.push(savingsAnnualAbsence3);
        var savingsAverageAbsence3 = this.savingAverage(savingsAnnualAbsence3);
        var savingsAnnualAbsence4 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 4);
        absenceCashFlow.push(savingsAnnualAbsence4);
        var savingsAverageAbsence4 = this.savingAverage(savingsAnnualAbsence4);
        var savingsAnnualAbsence5 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 5);
        absenceCashFlow.push(savingsAnnualAbsence5);
        var savingsAverageAbsence5 = this.savingAverage(savingsAnnualAbsence5);
        var savingsAnnualAbsence6 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 6);
        absenceCashFlow.push(savingsAnnualAbsence6);
        var savingsAverageAbsence6 = this.savingAverage(savingsAnnualAbsence6);
        var savingsAnnualAbsence7 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 7);
        absenceCashFlow.push(savingsAnnualAbsence7);
        var savingsAverageAbsence7 = this.savingAverage(savingsAnnualAbsence7);
        var savingsAnnualAbsence8 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 8);
        absenceCashFlow.push(savingsAnnualAbsence8);
        var savingsAverageAbsence8 = this.savingAverage(savingsAnnualAbsence8);
        var savingsAnnualAbsence9 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 9);
        absenceCashFlow.push(savingsAnnualAbsence9);
        var savingsAverageAbsence9 = this.savingAverage(savingsAnnualAbsence9);
        var savingsAnnualAbsence10 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 10);
        absenceCashFlow.push(savingsAnnualAbsence10);
        var savingsAverageAbsence10 = this.savingAverage(savingsAnnualAbsence10);
        var savingsAnnualPresence0 = this.startPresence - this.sixMonthPresence;
        presenceCashFlow.push(savingsAnnualPresence0);
        var savingsAveragePresence0 = this.savingAverage(savingsAnnualPresence0);
        var savingsAnnualPresence1 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 1);
        presenceCashFlow.push(savingsAnnualPresence1);
        var savingsAveragePresence1 = this.savingAverage(savingsAnnualPresence1);
        var savingsAnnualPresence2 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 2);
        presenceCashFlow.push(savingsAnnualPresence2);
        var savingsAveragePresence2 = this.savingAverage(savingsAnnualPresence2);
        var savingsAnnualPresence3 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 3);
        presenceCashFlow.push(savingsAnnualPresence3);
        var savingsAveragePresence3 = this.savingAverage(savingsAnnualPresence3);
        var savingsAnnualPresence4 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 4);
        presenceCashFlow.push(savingsAnnualPresence4);
        var savingsAveragePresence4 = this.savingAverage(savingsAnnualPresence4);
        var savingsAnnualPresence5 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 5);
        presenceCashFlow.push(savingsAnnualPresence5);
        var savingsAveragePresence5 = this.savingAverage(savingsAnnualPresence5);
        var savingsAnnualPresence6 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 6);
        presenceCashFlow.push(savingsAnnualPresence6);
        var savingsAveragePresence6 = this.savingAverage(savingsAnnualPresence6);
        var savingsAnnualPresence7 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 7);
        presenceCashFlow.push(savingsAnnualPresence7);
        var savingsAveragePresence7 = this.savingAverage(savingsAnnualPresence7);
        var savingsAnnualPresence8 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 8);
        presenceCashFlow.push(savingsAnnualPresence8);
        var savingsAveragePresence8 = this.savingAverage(savingsAnnualPresence8);
        var savingsAnnualPresence9 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 9);
        presenceCashFlow.push(savingsAnnualPresence9);
        var savingsAveragePresence9 = this.savingAverage(savingsAnnualPresence9);
        var savingsAnnualPresence10 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 10);
        presenceCashFlow.push(savingsAnnualPresence10);
        var savingsAveragePresence10 = this.savingAverage(savingsAnnualPresence10);
        var savingsAnnualMedical0 = this.startMedBenefit - this.sixMonthMedBenefit;
        medicalCashFlow.push(savingsAnnualMedical0);
        var savingsAverageMedical0 = this.savingAverage(savingsAnnualMedical0);
        var savingsAnnualMedical1 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 1);
        medicalCashFlow.push(savingsAnnualMedical1);
        var savingsAverageMedical1 = this.savingAverage(savingsAnnualMedical1);
        var savingsAnnualMedical2 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 2);
        medicalCashFlow.push(savingsAnnualMedical2);
        var savingsAverageMedical2 = this.savingAverage(savingsAnnualMedical2);
        var savingsAnnualMedical3 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 3);
        medicalCashFlow.push(savingsAnnualMedical3);
        var savingsAverageMedical3 = this.savingAverage(savingsAnnualMedical3);
        var savingsAnnualMedical4 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 4);
        medicalCashFlow.push(savingsAnnualMedical4);
        var savingsAverageMedical4 = this.savingAverage(savingsAnnualMedical4);
        var savingsAnnualMedical5 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 5);
        medicalCashFlow.push(savingsAnnualMedical5);
        var savingsAverageMedical5 = this.savingAverage(savingsAnnualMedical5);
        var savingsAnnualMedical6 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 6);
        medicalCashFlow.push(savingsAnnualMedical6);
        var savingsAverageMedical6 = this.savingAverage(savingsAnnualMedical6);
        var savingsAnnualMedical7 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 7);
        medicalCashFlow.push(savingsAnnualMedical7);
        var savingsAverageMedical7 = this.savingAverage(savingsAnnualMedical7);
        var savingsAnnualMedical8 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 8);
        medicalCashFlow.push(savingsAnnualMedical8);
        var savingsAverageMedical8 = this.savingAverage(savingsAnnualMedical8);
        var savingsAnnualMedical9 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 9);
        medicalCashFlow.push(savingsAnnualMedical9);
        var savingsAverageMedical9 = this.savingAverage(savingsAnnualMedical9);
        var savingsAnnualMedical10 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 10);
        medicalCashFlow.push(savingsAnnualMedical10);
        var savingsAverageMedical10 = this.savingAverage(savingsAnnualMedical10);
        var savingsAnnualDisable0 = this.startDisableBenefit - this.sixMonthDisableBenefit;
        disableCashFlow.push(savingsAnnualDisable0);
        var savingsAverageDisable0 = this.savingAverage(savingsAnnualDisable0);
        var savingsAnnualDisable1 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 1);
        disableCashFlow.push(savingsAnnualDisable1);
        var savingsAverageDisable1 = this.savingAverage(savingsAnnualDisable1);
        var savingsAnnualDisable2 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 2);
        disableCashFlow.push(savingsAnnualDisable2);
        var savingsAverageDisable2 = this.savingAverage(savingsAnnualDisable2);
        var savingsAnnualDisable3 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 3);
        disableCashFlow.push(savingsAnnualDisable3);
        var savingsAverageDisable3 = this.savingAverage(savingsAnnualDisable3);
        var savingsAnnualDisable4 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 4);
        disableCashFlow.push(savingsAnnualDisable4);
        var savingsAverageDisable4 = this.savingAverage(savingsAnnualDisable4);
        var savingsAnnualDisable5 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 5);
        disableCashFlow.push(savingsAnnualDisable5);
        var savingsAverageDisable5 = this.savingAverage(savingsAnnualDisable5);
        var savingsAnnualDisable6 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 6);
        disableCashFlow.push(savingsAnnualDisable6);
        var savingsAverageDisable6 = this.savingAverage(savingsAnnualDisable6);
        var savingsAnnualDisable7 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 7);
        disableCashFlow.push(savingsAnnualDisable7);
        var savingsAverageDisable7 = this.savingAverage(savingsAnnualDisable7);
        var savingsAnnualDisable8 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 8);
        disableCashFlow.push(savingsAnnualDisable8);
        var savingsAverageDisable8 = this.savingAverage(savingsAnnualDisable8);
        var savingsAnnualDisable9 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 9);
        disableCashFlow.push(savingsAnnualDisable9);
        var savingsAverageDisable9 = this.savingAverage(savingsAnnualDisable9);
        var savingsAnnualDisable10 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 10);
        disableCashFlow.push(savingsAnnualDisable10);
        var savingsAverageDisable10 = this.savingAverage(savingsAnnualDisable10);
        var savingsAnnualDeath0 = this.startDeathBenefit - this.sixMonthDeathBenefit;
        deathCashFlow.push(savingsAnnualDeath0);
        var savingsAverageDeath0 = this.savingAverage(savingsAnnualDeath0);
        var savingsAnnualDeath1 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 1);
        deathCashFlow.push(savingsAnnualDeath1);
        var savingsAverageDeath1 = this.savingAverage(savingsAnnualDeath1);
        var savingsAnnualDeath2 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 2);
        deathCashFlow.push(savingsAnnualDeath2);
        var savingsAverageDeath2 = this.savingAverage(savingsAnnualDeath2);
        var savingsAnnualDeath3 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 3);
        deathCashFlow.push(savingsAnnualDeath3);
        var savingsAverageDeath3 = this.savingAverage(savingsAnnualDeath3);
        var savingsAnnualDeath4 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 4);
        deathCashFlow.push(savingsAnnualDeath4);
        var savingsAverageDeath4 = this.savingAverage(savingsAnnualDeath4);
        var savingsAnnualDeath5 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 5);
        deathCashFlow.push(savingsAnnualDeath5);
        var savingsAverageDeath5 = this.savingAverage(savingsAnnualDeath5);
        var savingsAnnualDeath6 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 6);
        deathCashFlow.push(savingsAnnualDeath6);
        var savingsAverageDeath6 = this.savingAverage(savingsAnnualDeath6);
        var savingsAnnualDeath7 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 7);
        deathCashFlow.push(savingsAnnualDeath7);
        var savingsAverageDeath7 = this.savingAverage(savingsAnnualDeath0);
        var savingsAnnualDeath8 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 8);
        deathCashFlow.push(savingsAnnualDeath8);
        var savingsAverageDeath8 = this.savingAverage(savingsAnnualDeath8);
        var savingsAnnualDeath9 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 9);
        deathCashFlow.push(savingsAnnualDeath9);
        var savingsAverageDeath9 = this.savingAverage(savingsAnnualDeath9);
        var savingsAnnualDeath10 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 10);
        deathCashFlow.push(savingsAnnualDeath10);
        var savingsAverageDeath10 = this.savingAverage(savingsAnnualDeath10);
        var savingsAnnualTurnover0 = this.startTurnover - this.sixMonthTurnover;
        turnoverCashFlow.push(savingsAnnualTurnover0);
        var savingsAverageTurnover0 = this.savingAverage(savingsAnnualTurnover0);
        var savingsAnnualTurnover1 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 1);
        turnoverCashFlow.push(savingsAnnualTurnover1);
        var savingsAverageTurnover1 = this.savingAverage(savingsAnnualTurnover1);
        var savingsAnnualTurnover2 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 2);
        turnoverCashFlow.push(savingsAnnualTurnover2);
        var savingsAverageTurnover2 = this.savingAverage(savingsAnnualTurnover2);
        var savingsAnnualTurnover3 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 3);
        turnoverCashFlow.push(savingsAnnualTurnover3);
        var savingsAverageTurnover3 = this.savingAverage(savingsAnnualTurnover3);
        var savingsAnnualTurnover4 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 4);
        turnoverCashFlow.push(savingsAnnualTurnover4);
        var savingsAverageTurnover4 = this.savingAverage(savingsAnnualTurnover4);
        var savingsAnnualTurnover5 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 5);
        turnoverCashFlow.push(savingsAnnualTurnover5);
        var savingsAverageTurnover5 = this.savingAverage(savingsAnnualTurnover5);
        var savingsAnnualTurnover6 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 6);
        turnoverCashFlow.push(savingsAnnualTurnover6);
        var savingsAverageTurnover6 = this.savingAverage(savingsAnnualTurnover6);
        var savingsAnnualTurnover7 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 7);
        turnoverCashFlow.push(savingsAnnualTurnover7);
        var savingsAverageTurnover7 = this.savingAverage(savingsAnnualTurnover7);
        var savingsAnnualTurnover8 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 8);
        turnoverCashFlow.push(savingsAnnualTurnover8);
        var savingsAverageTurnover8 = this.savingAverage(savingsAnnualTurnover8);
        var savingsAnnualTurnover9 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 9);
        turnoverCashFlow.push(savingsAnnualTurnover9);
        var savingsAverageTurnover9 = this.savingAverage(savingsAnnualTurnover9);
        var savingsAnnualTurnover10 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 10);
        turnoverCashFlow.push(savingsAnnualTurnover10);
        var savingsAverageTurnover10 = this.savingAverage(savingsAnnualTurnover10);
        var savingsAnnualProductivity0 = this.sixMonthProductivity - this.startProductivity;
        productivityCashFlow.push(savingsAnnualProductivity0);
        var savingsAverageProductivity0 = this.savingAverage(savingsAnnualProductivity0);
        var savingsAnnualProductivity1 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 1);
        productivityCashFlow.push(savingsAnnualProductivity1);
        var savingsAverageProductivity1 = this.savingAverage(savingsAnnualProductivity1);
        var savingsAnnualProductivity2 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 2);
        productivityCashFlow.push(savingsAnnualProductivity2);
        var savingsAverageProductivity2 = this.savingAverage(savingsAnnualProductivity2);
        var savingsAnnualProductivity3 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 3);
        productivityCashFlow.push(savingsAnnualProductivity3);
        var savingsAverageProductivity3 = this.savingAverage(savingsAnnualProductivity3);
        var savingsAnnualProductivity4 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 4);
        productivityCashFlow.push(savingsAnnualProductivity4);
        var savingsAverageProductivity4 = this.savingAverage(savingsAnnualProductivity4);
        var savingsAnnualProductivity5 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 5);
        productivityCashFlow.push(savingsAnnualProductivity5);
        var savingsAverageProductivity5 = this.savingAverage(savingsAnnualProductivity5);
        var savingsAnnualProductivity6 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 6);
        productivityCashFlow.push(savingsAnnualProductivity6);
        var savingsAverageProductivity6 = this.savingAverage(savingsAnnualProductivity6);
        var savingsAnnualProductivity7 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 7);
        productivityCashFlow.push(savingsAnnualProductivity7);
        var savingsAverageProductivity7 = this.savingAverage(savingsAnnualProductivity7);
        var savingsAnnualProductivity8 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 8);
        productivityCashFlow.push(savingsAnnualProductivity8);
        var savingsAverageProductivity8 = this.savingAverage(savingsAnnualProductivity8);
        var savingsAnnualProductivity9 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 9);
        productivityCashFlow.push(savingsAnnualProductivity9);
        var savingsAverageProductivity9 = this.savingAverage(savingsAnnualProductivity9);
        var savingsAnnualProductivity10 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 10);
        productivityCashFlow.push(savingsAnnualProductivity10);
        var savingsAverageProductivity10 = this.savingAverage(savingsAnnualProductivity10);
        var npvAbsence0 = this.calculateNPVSavings(savingsAnnualAbsence0, this.npvCoeffYear0);
        var npvAbsence1 = this.calculateNPVSavings(savingsAnnualAbsence1, this.npvCoeffYear1);
        var npvAbsence2 = this.calculateNPVSavings(savingsAnnualAbsence2, this.npvCoeffYear2);
        var npvAbsence3 = this.calculateNPVSavings(savingsAnnualAbsence3, this.npvCoeffYear3);
        var npvAbsence4 = this.calculateNPVSavings(savingsAnnualAbsence4, this.npvCoeffYear4);
        var npvAbsence5 = this.calculateNPVSavings(savingsAnnualAbsence5, this.npvCoeffYear5);
        var npvAbsence6 = this.calculateNPVSavings(savingsAnnualAbsence6, this.npvCoeffYear6);
        var npvAbsence7 = this.calculateNPVSavings(savingsAnnualAbsence7, this.npvCoeffYear7);
        var npvAbsence8 = this.calculateNPVSavings(savingsAnnualAbsence8, this.npvCoeffYear8);
        var npvAbsence9 = this.calculateNPVSavings(savingsAnnualAbsence9, this.npvCoeffYear9);
        var npvAbsence10 = this.calculateNPVSavings(savingsAnnualAbsence10, this.npvCoeffYear10);
        var npvPresence0 = this.calculateNPVSavings(savingsAnnualPresence0, this.npvCoeffYear0);
        var npvPresence1 = this.calculateNPVSavings(savingsAnnualPresence1, this.npvCoeffYear1);
        var npvPresence2 = this.calculateNPVSavings(savingsAnnualPresence2, this.npvCoeffYear2);
        var npvPresence3 = this.calculateNPVSavings(savingsAnnualPresence3, this.npvCoeffYear3);
        var npvPresence4 = this.calculateNPVSavings(savingsAnnualPresence4, this.npvCoeffYear4);
        var npvPresence5 = this.calculateNPVSavings(savingsAnnualPresence5, this.npvCoeffYear5);
        var npvPresence6 = this.calculateNPVSavings(savingsAnnualPresence6, this.npvCoeffYear6);
        var npvPresence7 = this.calculateNPVSavings(savingsAnnualPresence7, this.npvCoeffYear7);
        var npvPresence8 = this.calculateNPVSavings(savingsAnnualPresence8, this.npvCoeffYear8);
        var npvPresence9 = this.calculateNPVSavings(savingsAnnualPresence9, this.npvCoeffYear9);
        var npvPresence10 = this.calculateNPVSavings(savingsAnnualPresence10, this.npvCoeffYear10);
        var npvMedical0 = this.calculateNPVSavings(savingsAnnualMedical0, this.npvCoeffYear0);
        var npvMedical1 = this.calculateNPVSavings(savingsAnnualMedical1, this.npvCoeffYear1);
        var npvMedical2 = this.calculateNPVSavings(savingsAnnualMedical2, this.npvCoeffYear2);
        var npvMedical3 = this.calculateNPVSavings(savingsAnnualMedical3, this.npvCoeffYear3);
        var npvMedical4 = this.calculateNPVSavings(savingsAnnualMedical4, this.npvCoeffYear4);
        var npvMedical5 = this.calculateNPVSavings(savingsAnnualMedical5, this.npvCoeffYear5);
        var npvMedical6 = this.calculateNPVSavings(savingsAnnualMedical6, this.npvCoeffYear6);
        var npvMedical7 = this.calculateNPVSavings(savingsAnnualMedical7, this.npvCoeffYear7);
        var npvMedical8 = this.calculateNPVSavings(savingsAnnualMedical8, this.npvCoeffYear8);
        var npvMedical9 = this.calculateNPVSavings(savingsAnnualMedical9, this.npvCoeffYear9);
        var npvMedical10 = this.calculateNPVSavings(savingsAnnualMedical10, this.npvCoeffYear10);
        var npvDisable0 = this.calculateNPVSavings(savingsAnnualDisable0, this.npvCoeffYear0);
        var npvDisable1 = this.calculateNPVSavings(savingsAnnualDisable1, this.npvCoeffYear1);
        var npvDisable2 = this.calculateNPVSavings(savingsAnnualDisable2, this.npvCoeffYear2);
        var npvDisable3 = this.calculateNPVSavings(savingsAnnualDisable3, this.npvCoeffYear3);
        var npvDisable4 = this.calculateNPVSavings(savingsAnnualDisable4, this.npvCoeffYear4);
        var npvDisable5 = this.calculateNPVSavings(savingsAnnualDisable5, this.npvCoeffYear5);
        var npvDisable6 = this.calculateNPVSavings(savingsAnnualDisable6, this.npvCoeffYear6);
        var npvDisable7 = this.calculateNPVSavings(savingsAnnualDisable7, this.npvCoeffYear7);
        var npvDisable8 = this.calculateNPVSavings(savingsAnnualDisable8, this.npvCoeffYear8);
        var npvDisable9 = this.calculateNPVSavings(savingsAnnualDisable9, this.npvCoeffYear9);
        var npvDisable10 = this.calculateNPVSavings(savingsAnnualDisable10, this.npvCoeffYear10);
        var npvDeath0 = this.calculateNPVSavings(savingsAnnualDeath0, this.npvCoeffYear0);
        var npvDeath1 = this.calculateNPVSavings(savingsAnnualDeath1, this.npvCoeffYear1);
        var npvDeath2 = this.calculateNPVSavings(savingsAnnualDeath2, this.npvCoeffYear2);
        var npvDeath3 = this.calculateNPVSavings(savingsAnnualDeath3, this.npvCoeffYear3);
        var npvDeath4 = this.calculateNPVSavings(savingsAnnualDeath4, this.npvCoeffYear4);
        var npvDeath5 = this.calculateNPVSavings(savingsAnnualDeath5, this.npvCoeffYear5);
        var npvDeath6 = this.calculateNPVSavings(savingsAnnualDeath6, this.npvCoeffYear6);
        var npvDeath7 = this.calculateNPVSavings(savingsAnnualDeath7, this.npvCoeffYear7);
        var npvDeath8 = this.calculateNPVSavings(savingsAnnualDeath8, this.npvCoeffYear8);
        var npvDeath9 = this.calculateNPVSavings(savingsAnnualDeath9, this.npvCoeffYear9);
        var npvDeath10 = this.calculateNPVSavings(savingsAnnualDeath10, this.npvCoeffYear10);
        var npvTurnover0 = this.calculateNPVSavings(savingsAnnualTurnover0, this.npvCoeffYear0);
        var npvTurnover1 = this.calculateNPVSavings(savingsAnnualTurnover1, this.npvCoeffYear1);
        var npvTurnover2 = this.calculateNPVSavings(savingsAnnualTurnover2, this.npvCoeffYear2);
        var npvTurnover3 = this.calculateNPVSavings(savingsAnnualTurnover3, this.npvCoeffYear3);
        var npvTurnover4 = this.calculateNPVSavings(savingsAnnualTurnover4, this.npvCoeffYear4);
        var npvTurnover5 = this.calculateNPVSavings(savingsAnnualTurnover5, this.npvCoeffYear5);
        var npvTurnover6 = this.calculateNPVSavings(savingsAnnualTurnover6, this.npvCoeffYear6);
        var npvTurnover7 = this.calculateNPVSavings(savingsAnnualTurnover7, this.npvCoeffYear7);
        var npvTurnover8 = this.calculateNPVSavings(savingsAnnualTurnover8, this.npvCoeffYear8);
        var npvTurnover9 = this.calculateNPVSavings(savingsAnnualTurnover9, this.npvCoeffYear9);
        var npvTurnover10 = this.calculateNPVSavings(savingsAnnualTurnover10, this.npvCoeffYear10);
        var npvProductivity0 = this.calculateNPVSavings(savingsAnnualProductivity0, this.npvCoeffYear0);
        var npvProductivity1 = this.calculateNPVSavings(savingsAnnualProductivity1, this.npvCoeffYear1);
        var npvProductivity2 = this.calculateNPVSavings(savingsAnnualProductivity2, this.npvCoeffYear2);
        var npvProductivity3 = this.calculateNPVSavings(savingsAnnualProductivity3, this.npvCoeffYear3);
        var npvProductivity4 = this.calculateNPVSavings(savingsAnnualProductivity4, this.npvCoeffYear4);
        var npvProductivity5 = this.calculateNPVSavings(savingsAnnualProductivity5, this.npvCoeffYear5);
        var npvProductivity6 = this.calculateNPVSavings(savingsAnnualProductivity6, this.npvCoeffYear6);
        var npvProductivity7 = this.calculateNPVSavings(savingsAnnualProductivity7, this.npvCoeffYear7);
        var npvProductivity8 = this.calculateNPVSavings(savingsAnnualProductivity8, this.npvCoeffYear8);
        var npvProductivity9 = this.calculateNPVSavings(savingsAnnualProductivity9, this.npvCoeffYear9);
        var npvProductivity10 = this.calculateNPVSavings(savingsAnnualProductivity10, this.npvCoeffYear10);
        var npvCosts0 = this.interventionCost * this.employeeNumber;
        var npvCosts1 = this.calculateNPVCosts(1);
        var npvCosts2 = this.calculateNPVCosts(2);
        var npvCosts3 = this.calculateNPVCosts(3);
        var npvCosts4 = this.calculateNPVCosts(4);
        var npvCosts5 = this.calculateNPVCosts(5);
        var npvCosts6 = this.calculateNPVCosts(6);
        var npvCosts7 = this.calculateNPVCosts(7);
        var npvCosts8 = this.calculateNPVCosts(8);
        var npvCosts9 = this.calculateNPVCosts(9);
        var npvCosts10 = this.calculateNPVCosts(10);
        this.updateElement('absence', this.startAbsence);
        this.updateElement('six-month-absence', this.sixMonthAbsence);
        this.updateElement('presence', this.startPresence);
        this.updateElement('six-month-presence', this.sixMonthPresence);
        this.updateElement('medical-benefit', this.startMedBenefit);
        this.updateElement('six-month-medical-benefit', this.sixMonthMedBenefit);
        this.updateElement('disable-benefit', this.startDisableBenefit);
        this.updateElement('six-month-disable-benefit', this.sixMonthDisableBenefit);
        this.updateElement('death-benefit', this.startDeathBenefit);
        this.updateElement('six-month-death-benefit', this.sixMonthDeathBenefit);
        this.updateElement('turnover', this.startTurnover);
        this.updateElement('six-month-turnover', this.sixMonthTurnover);
        this.updateElement('productivity', this.startProductivity);
        this.updateElement('six-month-productivity', this.sixMonthProductivity);
        this.updateElement('annual-savings-absence-year-0', this.savingAnnual(savingsAnnualAbsence0, null, 0));
        this.updateElement('annual-savings-absence-year-1', savingsAnnualAbsence1);
        this.updateElement('annual-savings-absence-year-2', savingsAnnualAbsence2);
        this.updateElement('annual-savings-absence-year-3', savingsAnnualAbsence3);
        this.updateElement('annual-savings-absence-year-4', savingsAnnualAbsence4);
        this.updateElement('annual-savings-absence-year-5', savingsAnnualAbsence5);
        this.updateElement('annual-savings-absence-year-6', savingsAnnualAbsence6);
        this.updateElement('annual-savings-absence-year-7', savingsAnnualAbsence7);
        this.updateElement('annual-savings-absence-year-8', savingsAnnualAbsence8);
        this.updateElement('annual-savings-absence-year-9', savingsAnnualAbsence9);
        this.updateElement('annual-savings-absence-year-10', savingsAnnualAbsence10);
        this.updateElement('annual-savings-presence-year-0', this.savingAnnual(savingsAnnualPresence0, null, 0));
        this.updateElement('annual-savings-presence-year-1', savingsAnnualPresence1);
        this.updateElement('annual-savings-presence-year-2', savingsAnnualPresence2);
        this.updateElement('annual-savings-presence-year-3', savingsAnnualPresence3);
        this.updateElement('annual-savings-presence-year-4', savingsAnnualPresence4);
        this.updateElement('annual-savings-presence-year-5', savingsAnnualPresence5);
        this.updateElement('annual-savings-presence-year-6', savingsAnnualPresence6);
        this.updateElement('annual-savings-presence-year-7', savingsAnnualPresence7);
        this.updateElement('annual-savings-presence-year-8', savingsAnnualPresence8);
        this.updateElement('annual-savings-presence-year-9', savingsAnnualPresence9);
        this.updateElement('annual-savings-presence-year-10', savingsAnnualPresence10);
        this.updateElement('annual-savings-medical-year-0', this.savingAnnual(savingsAnnualMedical0, null, 0));
        this.updateElement('annual-savings-medical-year-1', savingsAnnualMedical1);
        this.updateElement('annual-savings-medical-year-2', savingsAnnualMedical2);
        this.updateElement('annual-savings-medical-year-3', savingsAnnualMedical3);
        this.updateElement('annual-savings-medical-year-4', savingsAnnualMedical4);
        this.updateElement('annual-savings-medical-year-5', savingsAnnualMedical5);
        this.updateElement('annual-savings-medical-year-6', savingsAnnualMedical6);
        this.updateElement('annual-savings-medical-year-7', savingsAnnualMedical7);
        this.updateElement('annual-savings-medical-year-8', savingsAnnualMedical8);
        this.updateElement('annual-savings-medical-year-9', savingsAnnualMedical9);
        this.updateElement('annual-savings-medical-year-10', savingsAnnualMedical10);
        this.updateElement('annual-savings-disable-year-0', this.savingAnnual(savingsAnnualDisable0, null, 0));
        this.updateElement('annual-savings-disable-year-1', savingsAnnualDisable1);
        this.updateElement('annual-savings-disable-year-2', savingsAnnualDisable2);
        this.updateElement('annual-savings-disable-year-3', savingsAnnualDisable3);
        this.updateElement('annual-savings-disable-year-4', savingsAnnualDisable4);
        this.updateElement('annual-savings-disable-year-5', savingsAnnualDisable5);
        this.updateElement('annual-savings-disable-year-6', savingsAnnualDisable6);
        this.updateElement('annual-savings-disable-year-7', savingsAnnualDisable7);
        this.updateElement('annual-savings-disable-year-8', savingsAnnualDisable8);
        this.updateElement('annual-savings-disable-year-9', savingsAnnualDisable9);
        this.updateElement('annual-savings-disable-year-10', savingsAnnualDisable10);
        this.updateElement('annual-savings-death-year-0', this.savingAnnual(savingsAnnualDeath0, null, 0));
        this.updateElement('annual-savings-death-year-1', savingsAnnualDeath1);
        this.updateElement('annual-savings-death-year-2', savingsAnnualDeath2);
        this.updateElement('annual-savings-death-year-3', savingsAnnualDeath3);
        this.updateElement('annual-savings-death-year-4', savingsAnnualDeath4);
        this.updateElement('annual-savings-death-year-5', savingsAnnualDeath5);
        this.updateElement('annual-savings-death-year-6', savingsAnnualDeath6);
        this.updateElement('annual-savings-death-year-7', savingsAnnualDeath7);
        this.updateElement('annual-savings-death-year-8', savingsAnnualDeath8);
        this.updateElement('annual-savings-death-year-9', savingsAnnualDeath9);
        this.updateElement('annual-savings-death-year-10', savingsAnnualDeath10);
        this.updateElement('annual-savings-turnover-year-0', this.savingAnnual(savingsAnnualTurnover0, null, 0));
        this.updateElement('annual-savings-turnover-year-1', savingsAnnualTurnover1);
        this.updateElement('annual-savings-turnover-year-2', savingsAnnualTurnover2);
        this.updateElement('annual-savings-turnover-year-3', savingsAnnualTurnover3);
        this.updateElement('annual-savings-turnover-year-4', savingsAnnualTurnover4);
        this.updateElement('annual-savings-turnover-year-5', savingsAnnualTurnover5);
        this.updateElement('annual-savings-turnover-year-6', savingsAnnualTurnover6);
        this.updateElement('annual-savings-turnover-year-7', savingsAnnualTurnover7);
        this.updateElement('annual-savings-turnover-year-8', savingsAnnualTurnover8);
        this.updateElement('annual-savings-turnover-year-9', savingsAnnualTurnover9);
        this.updateElement('annual-savings-turnover-year-10', savingsAnnualTurnover10);
        this.updateElement('annual-savings-productivity-year-0', this.savingAnnual(savingsAnnualProductivity0, null, 0));
        this.updateElement('annual-savings-productivity-year-1', savingsAnnualProductivity1);
        this.updateElement('annual-savings-productivity-year-2', savingsAnnualProductivity2);
        this.updateElement('annual-savings-productivity-year-3', savingsAnnualProductivity3);
        this.updateElement('annual-savings-productivity-year-4', savingsAnnualProductivity4);
        this.updateElement('annual-savings-productivity-year-5', savingsAnnualProductivity5);
        this.updateElement('annual-savings-productivity-year-6', savingsAnnualProductivity6);
        this.updateElement('annual-savings-productivity-year-7', savingsAnnualProductivity7);
        this.updateElement('annual-savings-productivity-year-8', savingsAnnualProductivity8);
        this.updateElement('annual-savings-productivity-year-9', savingsAnnualProductivity9);
        this.updateElement('annual-savings-productivity-year-10', savingsAnnualProductivity10);
        this.updateElement('annual-total-year-0', this.total0);
        this.updateElement('annual-total-year-1', this.total1);
        this.updateElement('annual-total-year-2', this.total2);
        this.updateElement('annual-total-year-3', this.total3);
        this.updateElement('annual-total-year-4', this.total4);
        this.updateElement('annual-total-year-5', this.total5);
        this.updateElement('annual-total-year-6', this.total6);
        this.updateElement('annual-total-year-7', this.total7);
        this.updateElement('annual-total-year-8', this.total8);
        this.updateElement('annual-total-year-9', this.total9);
        this.updateElement('annual-total-year-10', this.total10);
        this.updateElement('average-savings-absence-year-0', savingsAverageAbsence0);
        this.updateElement('average-savings-absence-year-1', savingsAverageAbsence1);
        this.updateElement('average-savings-absence-year-2', savingsAverageAbsence2);
        this.updateElement('average-savings-absence-year-3', savingsAverageAbsence3);
        this.updateElement('average-savings-absence-year-4', savingsAverageAbsence4);
        this.updateElement('average-savings-absence-year-5', savingsAverageAbsence5);
        this.updateElement('average-savings-absence-year-6', savingsAverageAbsence6);
        this.updateElement('average-savings-absence-year-7', savingsAverageAbsence7);
        this.updateElement('average-savings-absence-year-8', savingsAverageAbsence8);
        this.updateElement('average-savings-absence-year-9', savingsAverageAbsence9);
        this.updateElement('average-savings-absence-year-10', savingsAverageAbsence10);
        this.updateElement('average-savings-presence-year-0', savingsAveragePresence0);
        this.updateElement('average-savings-presence-year-1', savingsAveragePresence1);
        this.updateElement('average-savings-presence-year-2', savingsAveragePresence2);
        this.updateElement('average-savings-presence-year-3', savingsAveragePresence3);
        this.updateElement('average-savings-presence-year-4', savingsAveragePresence4);
        this.updateElement('average-savings-presence-year-5', savingsAveragePresence5);
        this.updateElement('average-savings-presence-year-6', savingsAveragePresence6);
        this.updateElement('average-savings-presence-year-7', savingsAveragePresence7);
        this.updateElement('average-savings-presence-year-8', savingsAveragePresence8);
        this.updateElement('average-savings-presence-year-9', savingsAveragePresence9);
        this.updateElement('average-savings-presence-year-10', savingsAveragePresence10);
        this.updateElement('average-savings-medical-year-0', savingsAverageMedical0);
        this.updateElement('average-savings-medical-year-1', savingsAverageMedical1);
        this.updateElement('average-savings-medical-year-2', savingsAverageMedical2);
        this.updateElement('average-savings-medical-year-3', savingsAverageMedical3);
        this.updateElement('average-savings-medical-year-4', savingsAverageMedical4);
        this.updateElement('average-savings-medical-year-5', savingsAverageMedical5);
        this.updateElement('average-savings-medical-year-6', savingsAverageMedical6);
        this.updateElement('average-savings-medical-year-7', savingsAverageMedical7);
        this.updateElement('average-savings-medical-year-8', savingsAverageMedical8);
        this.updateElement('average-savings-medical-year-9', savingsAverageMedical9);
        this.updateElement('average-savings-medical-year-10', savingsAverageMedical10);
        this.updateElement('average-savings-disable-year-0', savingsAverageDisable0);
        this.updateElement('average-savings-disable-year-1', savingsAverageDisable1);
        this.updateElement('average-savings-disable-year-2', savingsAverageDisable2);
        this.updateElement('average-savings-disable-year-3', savingsAverageDisable3);
        this.updateElement('average-savings-disable-year-4', savingsAverageDisable4);
        this.updateElement('average-savings-disable-year-5', savingsAverageDisable5);
        this.updateElement('average-savings-disable-year-6', savingsAverageDisable6);
        this.updateElement('average-savings-disable-year-7', savingsAverageDisable7);
        this.updateElement('average-savings-disable-year-8', savingsAverageDisable8);
        this.updateElement('average-savings-disable-year-9', savingsAverageDisable9);
        this.updateElement('average-savings-disable-year-10', savingsAverageDisable10);
        this.updateElement('average-savings-death-year-0', savingsAverageDeath0);
        this.updateElement('average-savings-death-year-1', savingsAverageDeath1);
        this.updateElement('average-savings-death-year-2', savingsAverageDeath2);
        this.updateElement('average-savings-death-year-3', savingsAverageDeath3);
        this.updateElement('average-savings-death-year-4', savingsAverageDeath4);
        this.updateElement('average-savings-death-year-5', savingsAverageDeath5);
        this.updateElement('average-savings-death-year-6', savingsAverageDeath6);
        this.updateElement('average-savings-death-year-7', savingsAverageDeath7);
        this.updateElement('average-savings-death-year-8', savingsAverageDeath8);
        this.updateElement('average-savings-death-year-9', savingsAverageDeath9);
        this.updateElement('average-savings-death-year-10', savingsAverageDeath10);
        this.updateElement('average-savings-turnover-year-0', savingsAverageTurnover0);
        this.updateElement('average-savings-turnover-year-1', savingsAverageTurnover1);
        this.updateElement('average-savings-turnover-year-2', savingsAverageTurnover2);
        this.updateElement('average-savings-turnover-year-3', savingsAverageTurnover3);
        this.updateElement('average-savings-turnover-year-4', savingsAverageTurnover4);
        this.updateElement('average-savings-turnover-year-5', savingsAverageTurnover5);
        this.updateElement('average-savings-turnover-year-6', savingsAverageTurnover6);
        this.updateElement('average-savings-turnover-year-7', savingsAverageTurnover7);
        this.updateElement('average-savings-turnover-year-8', savingsAverageTurnover8);
        this.updateElement('average-savings-turnover-year-9', savingsAverageTurnover9);
        this.updateElement('average-savings-turnover-year-10', savingsAverageTurnover10);
        this.updateElement('average-savings-productivity-year-0', savingsAverageProductivity0);
        this.updateElement('average-savings-productivity-year-1', savingsAverageProductivity1);
        this.updateElement('average-savings-productivity-year-2', savingsAverageProductivity2);
        this.updateElement('average-savings-productivity-year-3', savingsAverageProductivity3);
        this.updateElement('average-savings-productivity-year-4', savingsAverageProductivity4);
        this.updateElement('average-savings-productivity-year-5', savingsAverageProductivity5);
        this.updateElement('average-savings-productivity-year-6', savingsAverageProductivity6);
        this.updateElement('average-savings-productivity-year-7', savingsAverageProductivity7);
        this.updateElement('average-savings-productivity-year-8', savingsAverageProductivity8);
        this.updateElement('average-savings-productivity-year-9', savingsAverageProductivity9);
        this.updateElement('average-savings-productivity-year-10', savingsAverageProductivity10);
        this.updateTotal('average-total-year-0', this.averageSavings0);
        this.updateTotal('average-total-year-1', this.averageSavings1);
        this.updateTotal('average-total-year-2', this.averageSavings2);
        this.updateTotal('average-total-year-3', this.averageSavings3);
        this.updateTotal('average-total-year-4', this.averageSavings4);
        this.updateTotal('average-total-year-5', this.averageSavings5);
        this.updateTotal('average-total-year-6', this.averageSavings6);
        this.updateTotal('average-total-year-7', this.averageSavings7);
        this.updateTotal('average-total-year-8', this.averageSavings8);
        this.updateTotal('average-total-year-9', this.averageSavings9);
        this.updateTotal('average-total-year-10', this.averageSavings10);
        this.updateElement('npv-absence-year-0', npvAbsence0);
        this.updateElement('npv-absence-year-1', npvAbsence1);
        this.updateElement('npv-absence-year-2', npvAbsence2);
        this.updateElement('npv-absence-year-3', npvAbsence3);
        this.updateElement('npv-absence-year-4', npvAbsence4);
        this.updateElement('npv-absence-year-5', npvAbsence5);
        this.updateElement('npv-absence-year-6', npvAbsence6);
        this.updateElement('npv-absence-year-7', npvAbsence7);
        this.updateElement('npv-absence-year-8', npvAbsence8);
        this.updateElement('npv-absence-year-9', npvAbsence9);
        this.updateElement('npv-absence-year-10', npvAbsence10);
        this.updateElement('npv-presence-year-0', npvPresence0);
        this.updateElement('npv-presence-year-1', npvPresence1);
        this.updateElement('npv-presence-year-2', npvPresence2);
        this.updateElement('npv-presence-year-3', npvPresence3);
        this.updateElement('npv-presence-year-4', npvPresence4);
        this.updateElement('npv-presence-year-5', npvPresence5);
        this.updateElement('npv-presence-year-6', npvPresence6);
        this.updateElement('npv-presence-year-7', npvPresence7);
        this.updateElement('npv-presence-year-8', npvPresence8);
        this.updateElement('npv-presence-year-9', npvPresence9);
        this.updateElement('npv-presence-year-10', npvPresence10);
        this.updateElement('npv-medical-year-0', npvMedical0);
        this.updateElement('npv-medical-year-1', npvMedical1);
        this.updateElement('npv-medical-year-2', npvMedical2);
        this.updateElement('npv-medical-year-3', npvMedical3);
        this.updateElement('npv-medical-year-4', npvMedical4);
        this.updateElement('npv-medical-year-5', npvMedical5);
        this.updateElement('npv-medical-year-6', npvMedical6);
        this.updateElement('npv-medical-year-7', npvMedical7);
        this.updateElement('npv-medical-year-8', npvMedical8);
        this.updateElement('npv-medical-year-9', npvMedical9);
        this.updateElement('npv-medical-year-10', npvMedical10);
        this.updateElement('npv-disable-year-0', npvDisable0);
        this.updateElement('npv-disable-year-1', npvDisable1);
        this.updateElement('npv-disable-year-2', npvDisable2);
        this.updateElement('npv-disable-year-3', npvDisable3);
        this.updateElement('npv-disable-year-4', npvDisable4);
        this.updateElement('npv-disable-year-5', npvDisable5);
        this.updateElement('npv-disable-year-6', npvDisable6);
        this.updateElement('npv-disable-year-7', npvDisable7);
        this.updateElement('npv-disable-year-8', npvDisable8);
        this.updateElement('npv-disable-year-9', npvDisable9);
        this.updateElement('npv-disable-year-10', npvDisable10);
        this.updateElement('npv-death-year-0', npvDeath0);
        this.updateElement('npv-death-year-1', npvDeath1);
        this.updateElement('npv-death-year-2', npvDeath2);
        this.updateElement('npv-death-year-3', npvDeath3);
        this.updateElement('npv-death-year-4', npvDeath4);
        this.updateElement('npv-death-year-5', npvDeath5);
        this.updateElement('npv-death-year-6', npvDeath6);
        this.updateElement('npv-death-year-7', npvDeath7);
        this.updateElement('npv-death-year-8', npvDeath8);
        this.updateElement('npv-death-year-9', npvDeath9);
        this.updateElement('npv-death-year-10', npvDeath10);
        this.updateElement('npv-turnover-year-0', npvTurnover0);
        this.updateElement('npv-turnover-year-1', npvTurnover1);
        this.updateElement('npv-turnover-year-2', npvTurnover2);
        this.updateElement('npv-turnover-year-3', npvTurnover3);
        this.updateElement('npv-turnover-year-4', npvTurnover4);
        this.updateElement('npv-turnover-year-5', npvTurnover5);
        this.updateElement('npv-turnover-year-6', npvTurnover6);
        this.updateElement('npv-turnover-year-7', npvTurnover7);
        this.updateElement('npv-turnover-year-8', npvTurnover8);
        this.updateElement('npv-turnover-year-9', npvTurnover9);
        this.updateElement('npv-turnover-year-10', npvTurnover10);
        this.updateElement('npv-productivity-year-0', npvProductivity0);
        this.updateElement('npv-productivity-year-1', npvProductivity1);
        this.updateElement('npv-productivity-year-2', npvProductivity2);
        this.updateElement('npv-productivity-year-3', npvProductivity3);
        this.updateElement('npv-productivity-year-4', npvProductivity4);
        this.updateElement('npv-productivity-year-5', npvProductivity5);
        this.updateElement('npv-productivity-year-6', npvProductivity6);
        this.updateElement('npv-productivity-year-7', npvProductivity7);
        this.updateElement('npv-productivity-year-8', npvProductivity8);
        this.updateElement('npv-productivity-year-9', npvProductivity9);
        this.updateElement('npv-productivity-year-10', npvProductivity10);
        this.updateTotal('npv-total-year-0', this.npvSavings0);
        this.updateTotal('npv-total-year-1', this.npvSavings1);
        this.updateTotal('npv-total-year-2', this.npvSavings2);
        this.updateTotal('npv-total-year-3', this.npvSavings3);
        this.updateTotal('npv-total-year-4', this.npvSavings4);
        this.updateTotal('npv-total-year-5', this.npvSavings5);
        this.updateTotal('npv-total-year-6', this.npvSavings6);
        this.updateTotal('npv-total-year-7', this.npvSavings7);
        this.updateTotal('npv-total-year-8', this.npvSavings8);
        this.updateTotal('npv-total-year-9', this.npvSavings9);
        this.updateTotal('npv-total-year-10', this.npvSavings10);
        this.updateElement('npv-costs-year-0', npvCosts0);
        this.updateElement('npv-costs-year-1', npvCosts1);
        this.updateElement('npv-costs-year-2', npvCosts2);
        this.updateElement('npv-costs-year-3', npvCosts3);
        this.updateElement('npv-costs-year-4', npvCosts4);
        this.updateElement('npv-costs-year-5', npvCosts5);
        this.updateElement('npv-costs-year-6', npvCosts6);
        this.updateElement('npv-costs-year-7', npvCosts7);
        this.updateElement('npv-costs-year-8', npvCosts8);
        this.updateElement('npv-costs-year-9', npvCosts9);
        this.updateElement('npv-costs-year-10', npvCosts10);
        var npvNetSavings0 = this.npvSavings0 - npvCosts0;
        var npvNetSavings1 = this.npvSavings1 - npvCosts1;
        var npvNetSavings2 = this.npvSavings2 - npvCosts2;
        var npvNetSavings3 = this.npvSavings3 - npvCosts3;
        var npvNetSavings4 = this.npvSavings4 - npvCosts4;
        var npvNetSavings5 = this.npvSavings5 - npvCosts5;
        var npvNetSavings6 = this.npvSavings6 - npvCosts6;
        var npvNetSavings7 = this.npvSavings7 - npvCosts7;
        var npvNetSavings8 = this.npvSavings8 - npvCosts8;
        var npvNetSavings9 = this.npvSavings9 - npvCosts9;
        var npvNetSavings10 = this.npvSavings10 - npvCosts10;
        this.updateElement('npv-savings-year-0', npvNetSavings0);
        this.updateElement('npv-savings-year-1', npvNetSavings1);
        this.updateElement('npv-savings-year-2', npvNetSavings2);
        this.updateElement('npv-savings-year-3', npvNetSavings3);
        this.updateElement('npv-savings-year-4', npvNetSavings4);
        this.updateElement('npv-savings-year-5', npvNetSavings5);
        this.updateElement('npv-savings-year-6', npvNetSavings6);
        this.updateElement('npv-savings-year-7', npvNetSavings7);
        this.updateElement('npv-savings-year-8', npvNetSavings8);
        this.updateElement('npv-savings-year-9', npvNetSavings9);
        this.updateElement('npv-savings-year-10', npvNetSavings10);
        var totalAbsence = this.getNPV(absenceCashFlow);
        var totalPresence = this.getNPV(presenceCashFlow);
        var totalMedical = this.getNPV(medicalCashFlow);
        var totalDisable = this.getNPV(disableCashFlow);
        var totalDeath = this.getNPV(deathCashFlow);
        var totalTurnover = this.getNPV(turnoverCashFlow);
        var totalProductivity = this.getNPV(productivityCashFlow);
        var totalSavings = totalAbsence + totalPresence + totalMedical + totalDisable + totalDeath + totalTurnover + totalProductivity;
        var totalInterventionCost = npvCosts0 + npvCosts1 + npvCosts2 + npvCosts3 + npvCosts4 + npvCosts5 + npvCosts6 + npvCosts7 + npvCosts8 + npvCosts9 + npvCosts10;
        this.updateElement('total-absence', totalAbsence);
        this.updateElement('per-employee-absence', totalAbsence / this.employeeNumber);
        this.updateElement('total-presence', totalPresence);
        this.updateElement('per-employee-presence', totalPresence / this.employeeNumber);
        this.updateElement('total-medical-benefit', totalMedical);
        this.updateElement('per-employee-medical-benefit', totalMedical / this.employeeNumber);
        this.updateElement('total-disable-benefit', totalDisable);
        this.updateElement('per-employee-disable-benefit', totalDisable / this.employeeNumber);
        this.updateElement('total-death-benefit', totalDeath);
        this.updateElement('per-employee-death-benefit', totalDeath / this.employeeNumber);
        this.updateElement('total-turnover', totalTurnover);
        this.updateElement('per-employee-turnover', totalTurnover / this.employeeNumber);
        this.updateElement('total-productivity', totalProductivity);
        this.updateElement('per-employee-productivity', totalProductivity / this.employeeNumber);
        this.updateElement('total-savings', totalSavings);
        this.updateElement('per-employee-savings', totalSavings / this.employeeNumber);
        this.updateElement('total-intervention-cost', totalInterventionCost);
        this.updateElement('per-employee-intervention-cost', totalInterventionCost / this.employeeNumber);
        this.updateElement('total-savings-wellness', totalSavings);
        this.updateElement('total-cost-wellness', totalInterventionCost);
        this.updateElement('net-savings-wellness', totalSavings - totalInterventionCost);
        this.updateElement('investment-ratio', totalSavings / totalInterventionCost);
        document.getElementById('investment-ratio').innerText = (totalSavings / totalInterventionCost).toFixed(2);
    };
    ROIAnalysis.prototype.addCurrency = function (amount) {
        return Math.round(amount).toLocaleString('en-GB', { style: 'currency', currency: 'GBP' });
    };
    ROIAnalysis.prototype.updateElement = function (element, value) {
        if (element.indexOf('average-savings') > -1) {
            if (element.indexOf('-0') > -1) {
                this.averageSavings0 += value;
            }
            if (element.indexOf('-1') > -1 && element.indexOf('-10') === -1) {
                this.averageSavings1 += value;
            }
            if (element.indexOf('-2') > -1) {
                this.averageSavings2 += value;
            }
            if (element.indexOf('-3') > -1) {
                this.averageSavings3 += value;
            }
            if (element.indexOf('-4') > -1) {
                this.averageSavings4 += value;
            }
            if (element.indexOf('-5') > -1) {
                this.averageSavings5 += value;
            }
            if (element.indexOf('-6') > -1) {
                this.averageSavings6 += value;
            }
            if (element.indexOf('-7') > -1) {
                this.averageSavings7 += value;
            }
            if (element.indexOf('-8') > -1) {
                this.averageSavings8 += value;
            }
            if (element.indexOf('-9') > -1) {
                this.averageSavings9 += value;
            }
            if (element.indexOf('-10') > -1) {
                this.averageSavings10 += value;
            }
        }
        if (element.indexOf('npv') > -1 && element.indexOf('savings') === -1 && element.indexOf('costs') === -1 && element.indexOf('total') === -1) {
            if (element.indexOf('-0') > -1) {
                this.npvSavings0 += value;
            }
            if (element.indexOf('-1') > -1 && element.indexOf('-10') === -1) {
                this.npvSavings1 += value;
            }
            if (element.indexOf('-2') > -1) {
                this.npvSavings2 += value;
            }
            if (element.indexOf('-3') > -1) {
                this.npvSavings3 += value;
            }
            if (element.indexOf('-4') > -1) {
                this.npvSavings4 += value;
            }
            if (element.indexOf('-5') > -1) {
                this.npvSavings5 += value;
            }
            if (element.indexOf('-6') > -1) {
                this.npvSavings6 += value;
            }
            if (element.indexOf('-7') > -1) {
                this.npvSavings7 += value;
            }
            if (element.indexOf('-8') > -1) {
                this.npvSavings8 += value;
            }
            if (element.indexOf('-9') > -1) {
                this.npvSavings9 += value;
            }
            if (element.indexOf('-10') > -1) {
                this.npvSavings10 += value;
            }
        }
        document.getElementById(element).innerText = this.addCurrency(value);
    };
    ROIAnalysis.prototype.updateTotal = function (element, value) {
        document.getElementById(element).innerText = this.addCurrency(value);
    };
    ROIAnalysis.prototype.savingAnnual = function (value, inflation, power) {
        var finalValue;
        if (power === 0) {
            this.total0 += value;
            return value;
        }
        finalValue = value * this.residualImpact * Math.pow(1 + inflation, power);
        if (power === 1) {
            this.total1 += finalValue;
        }
        if (power === 2) {
            this.total2 += finalValue;
        }
        if (power === 3) {
            this.total3 += finalValue;
        }
        if (power === 4) {
            this.total4 += finalValue;
        }
        if (power === 5) {
            this.total5 += finalValue;
        }
        if (power === 6) {
            this.total6 += finalValue;
        }
        if (power === 7) {
            this.total7 += finalValue;
        }
        if (power === 8) {
            this.total8 += finalValue;
        }
        if (power === 9) {
            this.total9 += finalValue;
        }
        if (power === 10) {
            this.total10 += finalValue;
        }
        return finalValue;
    };
    ROIAnalysis.prototype.savingAverage = function (value) {
        return value / this.employeeNumber;
    };
    ROIAnalysis.prototype.calculateNPVSavings = function (value, coefficient) {
        return value * Math.pow(1 + this.IRRforROI, -coefficient);
    };
    ROIAnalysis.prototype.calculateNPVCosts = function (coefficient) {
        return (this.employeeNumber * this.renewalServicesCost) / (Math.pow(1 + this.IRRforROI, coefficient));
    };
    ROIAnalysis.prototype.getNPV = function (cashFlows) {
        var npv = 0;
        for (var i = 0; i < cashFlows.length; i++) {
            npv += cashFlows[i] / Math.pow(this.IRRforROI + 1, i + 1);
        }
        return npv * Math.pow(1 + this.IRRforROI, 0.5);
    };
    ROIAnalysis.prototype.confirm = function () {
        this.employeeNumber = Number(this.employeesInput.value);
        this.avgPayroll = Number(this.payrollInput.value);
        this.interventionCost = Number(this.interventionCostInput.value);
        this.renewalServicesCost = Number(this.maintenanceCostInput.value);
        this.includeProductivityInput.checked ? this.includeProd = 'Yes' : this.includeProd = 'No';
        this.calculate();
        this.employeesInput.value = "";
        this.payrollInput.value = "";
        this.interventionCostInput.value = "";
        this.maintenanceCostInput.value = "";
        this.includeProductivityInput.checked = false;
    };
    ROIAnalysis.prototype.myFunction = function () {
        if (this.employeesInput.value && this.interventionCostInput.value && this.maintenanceCostInput.value) {
            this.btn.disabled = false;
        }
    };
    return ROIAnalysis;
}());
var analysis = new ROIAnalysis();
//# sourceMappingURL=app.js.map