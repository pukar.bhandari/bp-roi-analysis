class ROIAnalysis {
    //Dashboard
    employeeNumber: number = 100;
    avgPayroll: number = 12022; //currency
    interventionCost: number = 700; //currency
    renewalServicesCost: number = 300; //currency
    includeProd: string = 'Yes';
    includeLongImpact: string = 'No';
    includeWeightImpact: string = 'No';

//Assumptions
    defaultAvgPayroll: number = 50000; //currency
    avgAbsenceRate: number = 5.8 / 230 ; //percentage
    presentMultiplier: number = 300 / 100; //percentage
    medicalCost: number = 972; //currency
    medParticipation: number = 95 / 100; //percentage
    disabilityCostPercent: number = 0.8 / 100; //percentage
    disabilityParticipation: number = 90 / 100; //percentage
    deathCostPercent: number = 0.4 / 100; //percentage
    turnoverRate: number = 9.5 / 100; //percentage
    turnoverCost: number = 30 / 100; //percentage
    productivityMultiplier: number = 50 / 100; //percentage
    IRRforROI: number = 15 / 100; //percentage
    medicalInflation: number = 8 / 100; //percentage
    wageInflation: number = 2 / 100; //percentage

//impactSummary
    impactSummaryAbsence: number = 6.440683714 / 100; //percentage
    impactSummaryPresence: number = 3.839433258 / 100; //percentage
    impactSummaryMedical: number = 7.839229865 / 100; //percentage
    impactSummaryDisable: number = 8.497367014 / 100; //percentage
    impactSummaryDeath: number = 9.743985867 / 100; //percentage
    impactSummaryTurnover: number = 3.652593311 / 100; //percentage
    impactSummaryProductivity: number = - 4.19659943 / 100; //percentage
    residualImpact: number = 100 / 100; //percentage

//npv coefficients
    npvCoeffYear0: number = 0.5;
    npvCoeffYear1: number = 1.5;
    npvCoeffYear2: number = 2.5;
    npvCoeffYear3: number = 3.5;
    npvCoeffYear4: number = 4.5;
    npvCoeffYear5: number = 5.5;
    npvCoeffYear6: number = 6.5;
    npvCoeffYear7: number = 7.5;
    npvCoeffYear8: number = 8.5;
    npvCoeffYear9: number = 9.5;
    npvCoeffYear10: number = 10.5;

//variable initialisation
    payroll: number;
    startAbsence: number;
    sixMonthAbsence: number;
    startPresence: number;
    sixMonthPresence: number;
    startMedBenefit: number;
    sixMonthMedBenefit: number;
    startDisableBenefit: number;
    sixMonthDisableBenefit: number;
    startDeathBenefit: number;
    sixMonthDeathBenefit: number;
    startTurnover: number;
    sixMonthTurnover: number;
    startProductivity: number;
    sixMonthProductivity: number;

    total0: number;
    total1: number;
    total2: number;
    total3: number;
    total4: number;
    total5: number;
    total6: number;
    total7: number;
    total8: number;
    total9: number;
    total10: number;

    averageSavings0: number;
    averageSavings1: number;
    averageSavings2: number;
    averageSavings3: number;
    averageSavings4: number;
    averageSavings5: number;
    averageSavings6: number;
    averageSavings7: number;
    averageSavings8: number;
    averageSavings9: number;
    averageSavings10: number;

    npvSavings0: number = 0;
    npvSavings1: number = 0;
    npvSavings2: number = 0;
    npvSavings3: number = 0;
    npvSavings4: number = 0;
    npvSavings5: number = 0;
    npvSavings6: number = 0;
    npvSavings7: number = 0;
    npvSavings8: number = 0;
    npvSavings9: number = 0;
    npvSavings10: number = 0;

    btn = <HTMLButtonElement>document.getElementById('confirm');
    employeesInput = <HTMLInputElement>document.getElementById('employees');
    payrollInput = <HTMLInputElement>document.getElementById('payroll');
    interventionCostInput = <HTMLInputElement>document.getElementById('interventionCost');
    maintenanceCostInput = <HTMLInputElement>document.getElementById('maintenanceCost');
    includeProductivityInput = <HTMLInputElement>document.getElementById('includeProductivity');

    constructor() {
        this.btn.disabled = true;
        this.calculate();
    }

    calculate(): void {
        this.total0 = 0;
        this.total1 = 0;
        this.total2 = 0;
        this.total3 = 0;
        this.total4 = 0;
        this.total5 = 0;
        this.total6 = 0;
        this.total7 = 0;
        this.total8 = 0;
        this.total9 = 0;
        this.total10 = 0;

        this.averageSavings0 = 0;
        this.averageSavings1 = 0;
        this.averageSavings2 = 0;
        this.averageSavings3 = 0;
        this.averageSavings4 = 0;
        this.averageSavings5 = 0;
        this.averageSavings6 = 0;
        this.averageSavings7 = 0;
        this.averageSavings8 = 0;
        this.averageSavings9 = 0;
        this.averageSavings10 = 0;

        this.npvSavings0 = 0;
        this.npvSavings1 = 0;
        this.npvSavings2 = 0;
        this.npvSavings3 = 0;
        this.npvSavings4 = 0;
        this.npvSavings5 = 0;
        this.npvSavings6 = 0;
        this.npvSavings7 = 0;
        this.npvSavings8 = 0;
        this.npvSavings9 = 0;
        this.npvSavings10 = 0;

        let absenceCashFlow: Array<number> = [];
        let presenceCashFlow: Array<number> = [];
        let medicalCashFlow: Array<number> = [];
        let disableCashFlow: Array<number> = [];
        let deathCashFlow: Array<number> = [];
        let turnoverCashFlow: Array<number> = [];
        let productivityCashFlow: Array<number> = [];

        this.avgPayroll ? this.payroll = this.avgPayroll : this.payroll = this.defaultAvgPayroll;

        this.startAbsence =  this.payroll * this.avgAbsenceRate * this.employeeNumber;
        this.sixMonthAbsence = this.startAbsence * ( 1 - this.impactSummaryAbsence);

        this.startPresence = this.startAbsence * this.presentMultiplier;
        this.sixMonthPresence = this.startPresence * ( 1 - this.impactSummaryPresence);

        this.startMedBenefit = this.medicalCost * this.employeeNumber * this.medParticipation;
        this.sixMonthMedBenefit = this.startMedBenefit * ( 1 - this.impactSummaryMedical);

        this.startDisableBenefit = this.payroll * this.employeeNumber * this.disabilityParticipation * this.disabilityCostPercent;
        this.sixMonthDisableBenefit = this.startDisableBenefit * ( 1 - this.impactSummaryDisable);

        this.startDeathBenefit = this.deathCostPercent * this.payroll * this.employeeNumber;
        this.sixMonthDeathBenefit = this.startDeathBenefit * ( 1 - this.impactSummaryDeath);

        this.startTurnover = this.payroll * this.turnoverCost * this.employeeNumber * this.turnoverRate;
        this.sixMonthTurnover = this.startTurnover * ( 1 - this.impactSummaryTurnover);

        this.includeProd === "Yes" ? this.startProductivity = this.payroll * this.productivityMultiplier * this.employeeNumber : this.startProductivity = 0;
        this.sixMonthProductivity = this.startProductivity * (1 - this.impactSummaryProductivity);

        let savingsAnnualAbsence0 = this.startAbsence - this.sixMonthAbsence;
        absenceCashFlow.push(savingsAnnualAbsence0);
        let savingsAverageAbsence0 = this.savingAverage(savingsAnnualAbsence0);

        let savingsAnnualAbsence1 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 1);
        absenceCashFlow.push(savingsAnnualAbsence1);
        let savingsAverageAbsence1 = this.savingAverage(savingsAnnualAbsence1);

        let savingsAnnualAbsence2 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 2);
        absenceCashFlow.push(savingsAnnualAbsence2);
        let savingsAverageAbsence2 = this.savingAverage(savingsAnnualAbsence2);

        let savingsAnnualAbsence3 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 3);
        absenceCashFlow.push(savingsAnnualAbsence3);
        let savingsAverageAbsence3 = this.savingAverage(savingsAnnualAbsence3);

        let savingsAnnualAbsence4 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 4);
        absenceCashFlow.push(savingsAnnualAbsence4);
        let savingsAverageAbsence4 = this.savingAverage(savingsAnnualAbsence4);

        let savingsAnnualAbsence5 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 5);
        absenceCashFlow.push(savingsAnnualAbsence5);
        let savingsAverageAbsence5 = this.savingAverage(savingsAnnualAbsence5);

        let savingsAnnualAbsence6 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 6);
        absenceCashFlow.push(savingsAnnualAbsence6);
        let savingsAverageAbsence6 = this.savingAverage(savingsAnnualAbsence6);

        let savingsAnnualAbsence7 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 7);
        absenceCashFlow.push(savingsAnnualAbsence7);
        let savingsAverageAbsence7 = this.savingAverage(savingsAnnualAbsence7);

        let savingsAnnualAbsence8 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 8);
        absenceCashFlow.push(savingsAnnualAbsence8);
        let savingsAverageAbsence8 = this.savingAverage(savingsAnnualAbsence8);

        let savingsAnnualAbsence9 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 9);
        absenceCashFlow.push(savingsAnnualAbsence9);
        let savingsAverageAbsence9 = this.savingAverage(savingsAnnualAbsence9);

        let savingsAnnualAbsence10 = this.savingAnnual(savingsAnnualAbsence0, this.wageInflation, 10);
        absenceCashFlow.push(savingsAnnualAbsence10);
        let savingsAverageAbsence10 = this.savingAverage(savingsAnnualAbsence10);

        let savingsAnnualPresence0 = this.startPresence - this.sixMonthPresence;
        presenceCashFlow.push(savingsAnnualPresence0);
        let savingsAveragePresence0 = this.savingAverage(savingsAnnualPresence0);

        let savingsAnnualPresence1 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 1);
        presenceCashFlow.push(savingsAnnualPresence1);
        let savingsAveragePresence1 = this.savingAverage(savingsAnnualPresence1);

        let savingsAnnualPresence2 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 2);
        presenceCashFlow.push(savingsAnnualPresence2);
        let savingsAveragePresence2 = this.savingAverage(savingsAnnualPresence2);

        let savingsAnnualPresence3 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 3);
        presenceCashFlow.push(savingsAnnualPresence3);
        let savingsAveragePresence3 = this.savingAverage(savingsAnnualPresence3);

        let savingsAnnualPresence4 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 4);
        presenceCashFlow.push(savingsAnnualPresence4);
        let savingsAveragePresence4 = this.savingAverage(savingsAnnualPresence4);

        let savingsAnnualPresence5 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 5);
        presenceCashFlow.push(savingsAnnualPresence5);
        let savingsAveragePresence5 = this.savingAverage(savingsAnnualPresence5);

        let savingsAnnualPresence6 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 6);
        presenceCashFlow.push(savingsAnnualPresence6);
        let savingsAveragePresence6 = this.savingAverage(savingsAnnualPresence6);

        let savingsAnnualPresence7 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 7);
        presenceCashFlow.push(savingsAnnualPresence7);
        let savingsAveragePresence7 = this.savingAverage(savingsAnnualPresence7);

        let savingsAnnualPresence8 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 8);
        presenceCashFlow.push(savingsAnnualPresence8);
        let savingsAveragePresence8 = this.savingAverage(savingsAnnualPresence8);

        let savingsAnnualPresence9 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 9);
        presenceCashFlow.push(savingsAnnualPresence9);
        let savingsAveragePresence9 = this.savingAverage(savingsAnnualPresence9);

        let savingsAnnualPresence10 = this.savingAnnual(savingsAnnualPresence0, this.wageInflation, 10);
        presenceCashFlow.push(savingsAnnualPresence10);
        let savingsAveragePresence10 = this.savingAverage(savingsAnnualPresence10);

        let savingsAnnualMedical0 = this.startMedBenefit - this.sixMonthMedBenefit;
        medicalCashFlow.push(savingsAnnualMedical0);
        let savingsAverageMedical0 = this.savingAverage(savingsAnnualMedical0);

        let savingsAnnualMedical1 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 1);
        medicalCashFlow.push(savingsAnnualMedical1);
        let savingsAverageMedical1 = this.savingAverage(savingsAnnualMedical1);

        let savingsAnnualMedical2 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 2);
        medicalCashFlow.push(savingsAnnualMedical2);
        let savingsAverageMedical2 = this.savingAverage(savingsAnnualMedical2);

        let savingsAnnualMedical3 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 3);
        medicalCashFlow.push(savingsAnnualMedical3);
        let savingsAverageMedical3 = this.savingAverage(savingsAnnualMedical3);

        let savingsAnnualMedical4 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 4);
        medicalCashFlow.push(savingsAnnualMedical4);
        let savingsAverageMedical4 = this.savingAverage(savingsAnnualMedical4);

        let savingsAnnualMedical5 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 5);
        medicalCashFlow.push(savingsAnnualMedical5);
        let savingsAverageMedical5 = this.savingAverage(savingsAnnualMedical5);

        let savingsAnnualMedical6 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 6);
        medicalCashFlow.push(savingsAnnualMedical6);
        let savingsAverageMedical6 = this.savingAverage(savingsAnnualMedical6);

        let savingsAnnualMedical7 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 7);
        medicalCashFlow.push(savingsAnnualMedical7);
        let savingsAverageMedical7 = this.savingAverage(savingsAnnualMedical7);

        let savingsAnnualMedical8 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 8);
        medicalCashFlow.push(savingsAnnualMedical8);
        let savingsAverageMedical8 = this.savingAverage(savingsAnnualMedical8);

        let savingsAnnualMedical9 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 9);
        medicalCashFlow.push(savingsAnnualMedical9);
        let savingsAverageMedical9 = this.savingAverage(savingsAnnualMedical9);

        let savingsAnnualMedical10 = this.savingAnnual(savingsAnnualMedical0, this.medicalInflation, 10);
        medicalCashFlow.push(savingsAnnualMedical10);
        let savingsAverageMedical10 = this.savingAverage(savingsAnnualMedical10);

        let savingsAnnualDisable0 = this.startDisableBenefit - this.sixMonthDisableBenefit;
        disableCashFlow.push(savingsAnnualDisable0);
        let savingsAverageDisable0 = this.savingAverage(savingsAnnualDisable0);

        let savingsAnnualDisable1 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 1);
        disableCashFlow.push(savingsAnnualDisable1);
        let savingsAverageDisable1 = this.savingAverage(savingsAnnualDisable1);

        let savingsAnnualDisable2 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 2);
        disableCashFlow.push(savingsAnnualDisable2);
        let savingsAverageDisable2 = this.savingAverage(savingsAnnualDisable2);

        let savingsAnnualDisable3 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 3);
        disableCashFlow.push(savingsAnnualDisable3);
        let savingsAverageDisable3 = this.savingAverage(savingsAnnualDisable3);

        let savingsAnnualDisable4 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 4);
        disableCashFlow.push(savingsAnnualDisable4);
        let savingsAverageDisable4 = this.savingAverage(savingsAnnualDisable4);

        let savingsAnnualDisable5 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 5);
        disableCashFlow.push(savingsAnnualDisable5);
        let savingsAverageDisable5 = this.savingAverage(savingsAnnualDisable5);

        let savingsAnnualDisable6 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 6);
        disableCashFlow.push(savingsAnnualDisable6);
        let savingsAverageDisable6 = this.savingAverage(savingsAnnualDisable6);

        let savingsAnnualDisable7 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 7);
        disableCashFlow.push(savingsAnnualDisable7);
        let savingsAverageDisable7 = this.savingAverage(savingsAnnualDisable7);

        let savingsAnnualDisable8 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 8);
        disableCashFlow.push(savingsAnnualDisable8);
        let savingsAverageDisable8 = this.savingAverage(savingsAnnualDisable8);

        let savingsAnnualDisable9 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 9);
        disableCashFlow.push(savingsAnnualDisable9);
        let savingsAverageDisable9 = this.savingAverage(savingsAnnualDisable9);

        let savingsAnnualDisable10 = this.savingAnnual(savingsAnnualDisable0, this.wageInflation, 10);
        disableCashFlow.push(savingsAnnualDisable10);
        let savingsAverageDisable10 = this.savingAverage(savingsAnnualDisable10);

        let savingsAnnualDeath0 = this.startDeathBenefit - this.sixMonthDeathBenefit;
        deathCashFlow.push(savingsAnnualDeath0);
        let savingsAverageDeath0 = this.savingAverage(savingsAnnualDeath0);

        let savingsAnnualDeath1 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 1);
        deathCashFlow.push(savingsAnnualDeath1);
        let savingsAverageDeath1 = this.savingAverage(savingsAnnualDeath1);

        let savingsAnnualDeath2 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 2);
        deathCashFlow.push(savingsAnnualDeath2);
        let savingsAverageDeath2 = this.savingAverage(savingsAnnualDeath2);

        let savingsAnnualDeath3 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 3);
        deathCashFlow.push(savingsAnnualDeath3);
        let savingsAverageDeath3 = this.savingAverage(savingsAnnualDeath3);

        let savingsAnnualDeath4 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 4);
        deathCashFlow.push(savingsAnnualDeath4);
        let savingsAverageDeath4 = this.savingAverage(savingsAnnualDeath4);

        let savingsAnnualDeath5 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 5);
        deathCashFlow.push(savingsAnnualDeath5);
        let savingsAverageDeath5 = this.savingAverage(savingsAnnualDeath5);

        let savingsAnnualDeath6 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 6);
        deathCashFlow.push(savingsAnnualDeath6);
        let savingsAverageDeath6 = this.savingAverage(savingsAnnualDeath6);

        let savingsAnnualDeath7 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 7);
        deathCashFlow.push(savingsAnnualDeath7);
        let savingsAverageDeath7 = this.savingAverage(savingsAnnualDeath0);

        let savingsAnnualDeath8 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 8);
        deathCashFlow.push(savingsAnnualDeath8);
        let savingsAverageDeath8 = this.savingAverage(savingsAnnualDeath8);

        let savingsAnnualDeath9 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 9);
        deathCashFlow.push(savingsAnnualDeath9);
        let savingsAverageDeath9 = this.savingAverage(savingsAnnualDeath9);

        let savingsAnnualDeath10 = this.savingAnnual(savingsAnnualDeath0, this.wageInflation, 10);
        deathCashFlow.push(savingsAnnualDeath10);
        let savingsAverageDeath10 = this.savingAverage(savingsAnnualDeath10);

        let savingsAnnualTurnover0 = this.startTurnover - this.sixMonthTurnover;
        turnoverCashFlow.push(savingsAnnualTurnover0);
        let savingsAverageTurnover0 = this.savingAverage(savingsAnnualTurnover0);

        let savingsAnnualTurnover1 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 1);
        turnoverCashFlow.push(savingsAnnualTurnover1);
        let savingsAverageTurnover1 = this.savingAverage(savingsAnnualTurnover1);

        let savingsAnnualTurnover2 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 2);
        turnoverCashFlow.push(savingsAnnualTurnover2);
        let savingsAverageTurnover2 = this.savingAverage(savingsAnnualTurnover2);

        let savingsAnnualTurnover3 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 3);
        turnoverCashFlow.push(savingsAnnualTurnover3);
        let savingsAverageTurnover3 = this.savingAverage(savingsAnnualTurnover3);

        let savingsAnnualTurnover4 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 4);
        turnoverCashFlow.push(savingsAnnualTurnover4);
        let savingsAverageTurnover4 = this.savingAverage(savingsAnnualTurnover4);

        let savingsAnnualTurnover5 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 5);
        turnoverCashFlow.push(savingsAnnualTurnover5);
        let savingsAverageTurnover5 = this.savingAverage(savingsAnnualTurnover5);

        let savingsAnnualTurnover6 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 6);
        turnoverCashFlow.push(savingsAnnualTurnover6);
        let savingsAverageTurnover6 = this.savingAverage(savingsAnnualTurnover6);

        let savingsAnnualTurnover7 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 7);
        turnoverCashFlow.push(savingsAnnualTurnover7);
        let savingsAverageTurnover7 = this.savingAverage(savingsAnnualTurnover7);

        let savingsAnnualTurnover8 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 8);
        turnoverCashFlow.push(savingsAnnualTurnover8);
        let savingsAverageTurnover8 = this.savingAverage(savingsAnnualTurnover8);

        let savingsAnnualTurnover9 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 9);
        turnoverCashFlow.push(savingsAnnualTurnover9);
        let savingsAverageTurnover9 = this.savingAverage(savingsAnnualTurnover9);

        let savingsAnnualTurnover10 = this.savingAnnual(savingsAnnualTurnover0, this.wageInflation, 10);
        turnoverCashFlow.push(savingsAnnualTurnover10);
        let savingsAverageTurnover10 = this.savingAverage(savingsAnnualTurnover10);

        let savingsAnnualProductivity0 = this.sixMonthProductivity - this.startProductivity;
        productivityCashFlow.push(savingsAnnualProductivity0);
        let savingsAverageProductivity0 = this.savingAverage(savingsAnnualProductivity0);

        let savingsAnnualProductivity1 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 1);
        productivityCashFlow.push(savingsAnnualProductivity1);
        let savingsAverageProductivity1 = this.savingAverage(savingsAnnualProductivity1);

        let savingsAnnualProductivity2 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 2);
        productivityCashFlow.push(savingsAnnualProductivity2);
        let savingsAverageProductivity2 = this.savingAverage(savingsAnnualProductivity2);

        let savingsAnnualProductivity3 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 3);
        productivityCashFlow.push(savingsAnnualProductivity3);
        let savingsAverageProductivity3 = this.savingAverage(savingsAnnualProductivity3);

        let savingsAnnualProductivity4 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 4);
        productivityCashFlow.push(savingsAnnualProductivity4);
        let savingsAverageProductivity4 = this.savingAverage(savingsAnnualProductivity4);

        let savingsAnnualProductivity5 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 5);
        productivityCashFlow.push(savingsAnnualProductivity5);
        let savingsAverageProductivity5 = this.savingAverage(savingsAnnualProductivity5);

        let savingsAnnualProductivity6 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 6);
        productivityCashFlow.push(savingsAnnualProductivity6);
        let savingsAverageProductivity6 = this.savingAverage(savingsAnnualProductivity6);

        let savingsAnnualProductivity7 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 7);
        productivityCashFlow.push(savingsAnnualProductivity7);
        let savingsAverageProductivity7 = this.savingAverage(savingsAnnualProductivity7);

        let savingsAnnualProductivity8 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 8);
        productivityCashFlow.push(savingsAnnualProductivity8);
        let savingsAverageProductivity8 = this.savingAverage(savingsAnnualProductivity8);

        let savingsAnnualProductivity9 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 9);
        productivityCashFlow.push(savingsAnnualProductivity9);
        let savingsAverageProductivity9 = this.savingAverage(savingsAnnualProductivity9);

        let savingsAnnualProductivity10 = this.savingAnnual(savingsAnnualProductivity0, this.wageInflation, 10);
        productivityCashFlow.push(savingsAnnualProductivity10);
        let savingsAverageProductivity10 = this.savingAverage(savingsAnnualProductivity10);

        let npvAbsence0 = this.calculateNPVSavings(savingsAnnualAbsence0, this.npvCoeffYear0);
        let npvAbsence1 = this.calculateNPVSavings(savingsAnnualAbsence1, this.npvCoeffYear1);
        let npvAbsence2 = this.calculateNPVSavings(savingsAnnualAbsence2, this.npvCoeffYear2);
        let npvAbsence3 = this.calculateNPVSavings(savingsAnnualAbsence3, this.npvCoeffYear3);
        let npvAbsence4 = this.calculateNPVSavings(savingsAnnualAbsence4, this.npvCoeffYear4);
        let npvAbsence5 = this.calculateNPVSavings(savingsAnnualAbsence5, this.npvCoeffYear5);
        let npvAbsence6 = this.calculateNPVSavings(savingsAnnualAbsence6, this.npvCoeffYear6);
        let npvAbsence7 = this.calculateNPVSavings(savingsAnnualAbsence7, this.npvCoeffYear7);
        let npvAbsence8 = this.calculateNPVSavings(savingsAnnualAbsence8, this.npvCoeffYear8);
        let npvAbsence9 = this.calculateNPVSavings(savingsAnnualAbsence9, this.npvCoeffYear9);
        let npvAbsence10 = this.calculateNPVSavings(savingsAnnualAbsence10, this.npvCoeffYear10);

        let npvPresence0 = this.calculateNPVSavings(savingsAnnualPresence0, this.npvCoeffYear0);
        let npvPresence1 = this.calculateNPVSavings(savingsAnnualPresence1, this.npvCoeffYear1);
        let npvPresence2 = this.calculateNPVSavings(savingsAnnualPresence2, this.npvCoeffYear2);
        let npvPresence3 = this.calculateNPVSavings(savingsAnnualPresence3, this.npvCoeffYear3);
        let npvPresence4 = this.calculateNPVSavings(savingsAnnualPresence4, this.npvCoeffYear4);
        let npvPresence5 = this.calculateNPVSavings(savingsAnnualPresence5, this.npvCoeffYear5);
        let npvPresence6 = this.calculateNPVSavings(savingsAnnualPresence6, this.npvCoeffYear6);
        let npvPresence7 = this.calculateNPVSavings(savingsAnnualPresence7, this.npvCoeffYear7);
        let npvPresence8 = this.calculateNPVSavings(savingsAnnualPresence8, this.npvCoeffYear8);
        let npvPresence9 = this.calculateNPVSavings(savingsAnnualPresence9, this.npvCoeffYear9);
        let npvPresence10 = this.calculateNPVSavings(savingsAnnualPresence10, this.npvCoeffYear10);

        let npvMedical0 = this.calculateNPVSavings(savingsAnnualMedical0, this.npvCoeffYear0);
        let npvMedical1 = this.calculateNPVSavings(savingsAnnualMedical1, this.npvCoeffYear1);
        let npvMedical2 = this.calculateNPVSavings(savingsAnnualMedical2, this.npvCoeffYear2);
        let npvMedical3 = this.calculateNPVSavings(savingsAnnualMedical3, this.npvCoeffYear3);
        let npvMedical4 = this.calculateNPVSavings(savingsAnnualMedical4, this.npvCoeffYear4);
        let npvMedical5 = this.calculateNPVSavings(savingsAnnualMedical5, this.npvCoeffYear5);
        let npvMedical6 = this.calculateNPVSavings(savingsAnnualMedical6, this.npvCoeffYear6);
        let npvMedical7 = this.calculateNPVSavings(savingsAnnualMedical7, this.npvCoeffYear7);
        let npvMedical8 = this.calculateNPVSavings(savingsAnnualMedical8, this.npvCoeffYear8);
        let npvMedical9 = this.calculateNPVSavings(savingsAnnualMedical9, this.npvCoeffYear9);
        let npvMedical10 = this.calculateNPVSavings(savingsAnnualMedical10, this.npvCoeffYear10);

        let npvDisable0 = this.calculateNPVSavings(savingsAnnualDisable0, this.npvCoeffYear0);
        let npvDisable1 = this.calculateNPVSavings(savingsAnnualDisable1, this.npvCoeffYear1);
        let npvDisable2 = this.calculateNPVSavings(savingsAnnualDisable2, this.npvCoeffYear2);
        let npvDisable3 = this.calculateNPVSavings(savingsAnnualDisable3, this.npvCoeffYear3);
        let npvDisable4 = this.calculateNPVSavings(savingsAnnualDisable4, this.npvCoeffYear4);
        let npvDisable5 = this.calculateNPVSavings(savingsAnnualDisable5, this.npvCoeffYear5);
        let npvDisable6 = this.calculateNPVSavings(savingsAnnualDisable6, this.npvCoeffYear6);
        let npvDisable7 = this.calculateNPVSavings(savingsAnnualDisable7, this.npvCoeffYear7);
        let npvDisable8 = this.calculateNPVSavings(savingsAnnualDisable8, this.npvCoeffYear8);
        let npvDisable9 = this.calculateNPVSavings(savingsAnnualDisable9, this.npvCoeffYear9);
        let npvDisable10 = this.calculateNPVSavings(savingsAnnualDisable10, this.npvCoeffYear10);

        let npvDeath0 = this.calculateNPVSavings(savingsAnnualDeath0, this.npvCoeffYear0);
        let npvDeath1 = this.calculateNPVSavings(savingsAnnualDeath1, this.npvCoeffYear1);
        let npvDeath2 = this.calculateNPVSavings(savingsAnnualDeath2, this.npvCoeffYear2);
        let npvDeath3 = this.calculateNPVSavings(savingsAnnualDeath3, this.npvCoeffYear3);
        let npvDeath4 = this.calculateNPVSavings(savingsAnnualDeath4, this.npvCoeffYear4);
        let npvDeath5 = this.calculateNPVSavings(savingsAnnualDeath5, this.npvCoeffYear5);
        let npvDeath6 = this.calculateNPVSavings(savingsAnnualDeath6, this.npvCoeffYear6);
        let npvDeath7 = this.calculateNPVSavings(savingsAnnualDeath7, this.npvCoeffYear7);
        let npvDeath8 = this.calculateNPVSavings(savingsAnnualDeath8, this.npvCoeffYear8);
        let npvDeath9 = this.calculateNPVSavings(savingsAnnualDeath9, this.npvCoeffYear9);
        let npvDeath10 = this.calculateNPVSavings(savingsAnnualDeath10, this.npvCoeffYear10);

        let npvTurnover0 = this.calculateNPVSavings(savingsAnnualTurnover0, this.npvCoeffYear0);
        let npvTurnover1 = this.calculateNPVSavings(savingsAnnualTurnover1, this.npvCoeffYear1);
        let npvTurnover2 = this.calculateNPVSavings(savingsAnnualTurnover2, this.npvCoeffYear2);
        let npvTurnover3 = this.calculateNPVSavings(savingsAnnualTurnover3, this.npvCoeffYear3);
        let npvTurnover4 = this.calculateNPVSavings(savingsAnnualTurnover4, this.npvCoeffYear4);
        let npvTurnover5 = this.calculateNPVSavings(savingsAnnualTurnover5, this.npvCoeffYear5);
        let npvTurnover6 = this.calculateNPVSavings(savingsAnnualTurnover6, this.npvCoeffYear6);
        let npvTurnover7 = this.calculateNPVSavings(savingsAnnualTurnover7, this.npvCoeffYear7);
        let npvTurnover8 = this.calculateNPVSavings(savingsAnnualTurnover8, this.npvCoeffYear8);
        let npvTurnover9 = this.calculateNPVSavings(savingsAnnualTurnover9, this.npvCoeffYear9);
        let npvTurnover10 = this.calculateNPVSavings(savingsAnnualTurnover10, this.npvCoeffYear10);

        let npvProductivity0 = this.calculateNPVSavings(savingsAnnualProductivity0, this.npvCoeffYear0);
        let npvProductivity1 = this.calculateNPVSavings(savingsAnnualProductivity1, this.npvCoeffYear1);
        let npvProductivity2 = this.calculateNPVSavings(savingsAnnualProductivity2, this.npvCoeffYear2);
        let npvProductivity3 = this.calculateNPVSavings(savingsAnnualProductivity3, this.npvCoeffYear3);
        let npvProductivity4 = this.calculateNPVSavings(savingsAnnualProductivity4, this.npvCoeffYear4);
        let npvProductivity5 = this.calculateNPVSavings(savingsAnnualProductivity5, this.npvCoeffYear5);
        let npvProductivity6 = this.calculateNPVSavings(savingsAnnualProductivity6, this.npvCoeffYear6);
        let npvProductivity7 = this.calculateNPVSavings(savingsAnnualProductivity7, this.npvCoeffYear7);
        let npvProductivity8 = this.calculateNPVSavings(savingsAnnualProductivity8, this.npvCoeffYear8);
        let npvProductivity9 = this.calculateNPVSavings(savingsAnnualProductivity9, this.npvCoeffYear9);
        let npvProductivity10 = this.calculateNPVSavings(savingsAnnualProductivity10, this.npvCoeffYear10);

        let npvCosts0 = this.interventionCost * this.employeeNumber;
        let npvCosts1 = this.calculateNPVCosts(1);
        let npvCosts2 = this.calculateNPVCosts(2);
        let npvCosts3 = this.calculateNPVCosts(3);
        let npvCosts4 = this.calculateNPVCosts(4);
        let npvCosts5 = this.calculateNPVCosts(5);
        let npvCosts6 = this.calculateNPVCosts(6);
        let npvCosts7 = this.calculateNPVCosts(7);
        let npvCosts8 = this.calculateNPVCosts(8);
        let npvCosts9 = this.calculateNPVCosts(9);
        let npvCosts10 = this.calculateNPVCosts(10);

        this.updateElement('absence', this.startAbsence);
        this.updateElement('six-month-absence', this.sixMonthAbsence);
        this.updateElement('presence', this.startPresence);
        this.updateElement('six-month-presence', this.sixMonthPresence);
        this.updateElement('medical-benefit', this.startMedBenefit);
        this.updateElement('six-month-medical-benefit', this.sixMonthMedBenefit);
        this.updateElement('disable-benefit', this.startDisableBenefit);
        this.updateElement('six-month-disable-benefit', this.sixMonthDisableBenefit);
        this.updateElement('death-benefit', this.startDeathBenefit);
        this.updateElement('six-month-death-benefit', this.sixMonthDeathBenefit);
        this.updateElement('turnover', this.startTurnover);
        this.updateElement('six-month-turnover', this.sixMonthTurnover);
        this.updateElement('productivity', this.startProductivity);
        this.updateElement('six-month-productivity', this.sixMonthProductivity);

        this.updateElement('annual-savings-absence-year-0', this.savingAnnual(savingsAnnualAbsence0, null, 0));
        this.updateElement('annual-savings-absence-year-1', savingsAnnualAbsence1);
        this.updateElement('annual-savings-absence-year-2', savingsAnnualAbsence2);
        this.updateElement('annual-savings-absence-year-3', savingsAnnualAbsence3);
        this.updateElement('annual-savings-absence-year-4', savingsAnnualAbsence4);
        this.updateElement('annual-savings-absence-year-5', savingsAnnualAbsence5);
        this.updateElement('annual-savings-absence-year-6', savingsAnnualAbsence6);
        this.updateElement('annual-savings-absence-year-7', savingsAnnualAbsence7);
        this.updateElement('annual-savings-absence-year-8', savingsAnnualAbsence8);
        this.updateElement('annual-savings-absence-year-9', savingsAnnualAbsence9);
        this.updateElement('annual-savings-absence-year-10', savingsAnnualAbsence10);

        this.updateElement('annual-savings-presence-year-0', this.savingAnnual(savingsAnnualPresence0, null,0));
        this.updateElement('annual-savings-presence-year-1', savingsAnnualPresence1);
        this.updateElement('annual-savings-presence-year-2', savingsAnnualPresence2);
        this.updateElement('annual-savings-presence-year-3', savingsAnnualPresence3);
        this.updateElement('annual-savings-presence-year-4', savingsAnnualPresence4);
        this.updateElement('annual-savings-presence-year-5', savingsAnnualPresence5);
        this.updateElement('annual-savings-presence-year-6', savingsAnnualPresence6);
        this.updateElement('annual-savings-presence-year-7', savingsAnnualPresence7);
        this.updateElement('annual-savings-presence-year-8', savingsAnnualPresence8);
        this.updateElement('annual-savings-presence-year-9', savingsAnnualPresence9);
        this.updateElement('annual-savings-presence-year-10', savingsAnnualPresence10);

        this.updateElement('annual-savings-medical-year-0', this.savingAnnual(savingsAnnualMedical0, null, 0));
        this.updateElement('annual-savings-medical-year-1', savingsAnnualMedical1);
        this.updateElement('annual-savings-medical-year-2', savingsAnnualMedical2);
        this.updateElement('annual-savings-medical-year-3', savingsAnnualMedical3);
        this.updateElement('annual-savings-medical-year-4', savingsAnnualMedical4);
        this.updateElement('annual-savings-medical-year-5', savingsAnnualMedical5);
        this.updateElement('annual-savings-medical-year-6', savingsAnnualMedical6);
        this.updateElement('annual-savings-medical-year-7', savingsAnnualMedical7);
        this.updateElement('annual-savings-medical-year-8', savingsAnnualMedical8);
        this.updateElement('annual-savings-medical-year-9', savingsAnnualMedical9);
        this.updateElement('annual-savings-medical-year-10', savingsAnnualMedical10);

        this.updateElement('annual-savings-disable-year-0', this.savingAnnual(savingsAnnualDisable0, null, 0));
        this.updateElement('annual-savings-disable-year-1', savingsAnnualDisable1);
        this.updateElement('annual-savings-disable-year-2', savingsAnnualDisable2);
        this.updateElement('annual-savings-disable-year-3', savingsAnnualDisable3);
        this.updateElement('annual-savings-disable-year-4', savingsAnnualDisable4);
        this.updateElement('annual-savings-disable-year-5', savingsAnnualDisable5);
        this.updateElement('annual-savings-disable-year-6', savingsAnnualDisable6);
        this.updateElement('annual-savings-disable-year-7', savingsAnnualDisable7);
        this.updateElement('annual-savings-disable-year-8', savingsAnnualDisable8);
        this.updateElement('annual-savings-disable-year-9', savingsAnnualDisable9);
        this.updateElement('annual-savings-disable-year-10', savingsAnnualDisable10);

        this.updateElement('annual-savings-death-year-0', this.savingAnnual(savingsAnnualDeath0, null, 0));
        this.updateElement('annual-savings-death-year-1', savingsAnnualDeath1);
        this.updateElement('annual-savings-death-year-2', savingsAnnualDeath2);
        this.updateElement('annual-savings-death-year-3', savingsAnnualDeath3);
        this.updateElement('annual-savings-death-year-4', savingsAnnualDeath4);
        this.updateElement('annual-savings-death-year-5', savingsAnnualDeath5);
        this.updateElement('annual-savings-death-year-6', savingsAnnualDeath6);
        this.updateElement('annual-savings-death-year-7', savingsAnnualDeath7);
        this.updateElement('annual-savings-death-year-8', savingsAnnualDeath8);
        this.updateElement('annual-savings-death-year-9', savingsAnnualDeath9);
        this.updateElement('annual-savings-death-year-10', savingsAnnualDeath10);

        this.updateElement('annual-savings-turnover-year-0', this.savingAnnual(savingsAnnualTurnover0, null, 0));
        this.updateElement('annual-savings-turnover-year-1', savingsAnnualTurnover1);
        this.updateElement('annual-savings-turnover-year-2', savingsAnnualTurnover2);
        this.updateElement('annual-savings-turnover-year-3', savingsAnnualTurnover3);
        this.updateElement('annual-savings-turnover-year-4', savingsAnnualTurnover4);
        this.updateElement('annual-savings-turnover-year-5', savingsAnnualTurnover5);
        this.updateElement('annual-savings-turnover-year-6', savingsAnnualTurnover6);
        this.updateElement('annual-savings-turnover-year-7', savingsAnnualTurnover7);
        this.updateElement('annual-savings-turnover-year-8', savingsAnnualTurnover8);
        this.updateElement('annual-savings-turnover-year-9', savingsAnnualTurnover9);
        this.updateElement('annual-savings-turnover-year-10', savingsAnnualTurnover10);

        this.updateElement('annual-savings-productivity-year-0', this.savingAnnual(savingsAnnualProductivity0, null, 0));
        this.updateElement('annual-savings-productivity-year-1', savingsAnnualProductivity1);
        this.updateElement('annual-savings-productivity-year-2', savingsAnnualProductivity2);
        this.updateElement('annual-savings-productivity-year-3', savingsAnnualProductivity3);
        this.updateElement('annual-savings-productivity-year-4', savingsAnnualProductivity4);
        this.updateElement('annual-savings-productivity-year-5', savingsAnnualProductivity5);
        this.updateElement('annual-savings-productivity-year-6', savingsAnnualProductivity6);
        this.updateElement('annual-savings-productivity-year-7', savingsAnnualProductivity7);
        this.updateElement('annual-savings-productivity-year-8', savingsAnnualProductivity8);
        this.updateElement('annual-savings-productivity-year-9', savingsAnnualProductivity9);
        this.updateElement('annual-savings-productivity-year-10', savingsAnnualProductivity10);

        this.updateElement('annual-total-year-0', this.total0);
        this.updateElement('annual-total-year-1', this.total1);
        this.updateElement('annual-total-year-2', this.total2);
        this.updateElement('annual-total-year-3', this.total3);
        this.updateElement('annual-total-year-4', this.total4);
        this.updateElement('annual-total-year-5', this.total5);
        this.updateElement('annual-total-year-6', this.total6);
        this.updateElement('annual-total-year-7', this.total7);
        this.updateElement('annual-total-year-8', this.total8);
        this.updateElement('annual-total-year-9', this.total9);
        this.updateElement('annual-total-year-10', this.total10);

        this.updateElement('average-savings-absence-year-0', savingsAverageAbsence0);
        this.updateElement('average-savings-absence-year-1', savingsAverageAbsence1);
        this.updateElement('average-savings-absence-year-2', savingsAverageAbsence2);
        this.updateElement('average-savings-absence-year-3', savingsAverageAbsence3);
        this.updateElement('average-savings-absence-year-4', savingsAverageAbsence4);
        this.updateElement('average-savings-absence-year-5', savingsAverageAbsence5);
        this.updateElement('average-savings-absence-year-6', savingsAverageAbsence6);
        this.updateElement('average-savings-absence-year-7', savingsAverageAbsence7);
        this.updateElement('average-savings-absence-year-8', savingsAverageAbsence8);
        this.updateElement('average-savings-absence-year-9', savingsAverageAbsence9);
        this.updateElement('average-savings-absence-year-10', savingsAverageAbsence10);

        this.updateElement('average-savings-presence-year-0', savingsAveragePresence0);
        this.updateElement('average-savings-presence-year-1', savingsAveragePresence1);
        this.updateElement('average-savings-presence-year-2', savingsAveragePresence2);
        this.updateElement('average-savings-presence-year-3', savingsAveragePresence3);
        this.updateElement('average-savings-presence-year-4', savingsAveragePresence4);
        this.updateElement('average-savings-presence-year-5', savingsAveragePresence5);
        this.updateElement('average-savings-presence-year-6', savingsAveragePresence6);
        this.updateElement('average-savings-presence-year-7', savingsAveragePresence7);
        this.updateElement('average-savings-presence-year-8', savingsAveragePresence8);
        this.updateElement('average-savings-presence-year-9', savingsAveragePresence9);
        this.updateElement('average-savings-presence-year-10', savingsAveragePresence10);

        this.updateElement('average-savings-medical-year-0', savingsAverageMedical0);
        this.updateElement('average-savings-medical-year-1', savingsAverageMedical1);
        this.updateElement('average-savings-medical-year-2', savingsAverageMedical2);
        this.updateElement('average-savings-medical-year-3', savingsAverageMedical3);
        this.updateElement('average-savings-medical-year-4', savingsAverageMedical4);
        this.updateElement('average-savings-medical-year-5', savingsAverageMedical5);
        this.updateElement('average-savings-medical-year-6', savingsAverageMedical6);
        this.updateElement('average-savings-medical-year-7', savingsAverageMedical7);
        this.updateElement('average-savings-medical-year-8', savingsAverageMedical8);
        this.updateElement('average-savings-medical-year-9', savingsAverageMedical9);
        this.updateElement('average-savings-medical-year-10', savingsAverageMedical10);

        this.updateElement('average-savings-disable-year-0', savingsAverageDisable0);
        this.updateElement('average-savings-disable-year-1', savingsAverageDisable1);
        this.updateElement('average-savings-disable-year-2', savingsAverageDisable2);
        this.updateElement('average-savings-disable-year-3', savingsAverageDisable3);
        this.updateElement('average-savings-disable-year-4', savingsAverageDisable4);
        this.updateElement('average-savings-disable-year-5', savingsAverageDisable5);
        this.updateElement('average-savings-disable-year-6', savingsAverageDisable6);
        this.updateElement('average-savings-disable-year-7', savingsAverageDisable7);
        this.updateElement('average-savings-disable-year-8', savingsAverageDisable8);
        this.updateElement('average-savings-disable-year-9', savingsAverageDisable9);
        this.updateElement('average-savings-disable-year-10', savingsAverageDisable10);

        this.updateElement('average-savings-death-year-0', savingsAverageDeath0);
        this.updateElement('average-savings-death-year-1', savingsAverageDeath1);
        this.updateElement('average-savings-death-year-2', savingsAverageDeath2);
        this.updateElement('average-savings-death-year-3', savingsAverageDeath3);
        this.updateElement('average-savings-death-year-4', savingsAverageDeath4);
        this.updateElement('average-savings-death-year-5', savingsAverageDeath5);
        this.updateElement('average-savings-death-year-6', savingsAverageDeath6);
        this.updateElement('average-savings-death-year-7', savingsAverageDeath7);
        this.updateElement('average-savings-death-year-8', savingsAverageDeath8);
        this.updateElement('average-savings-death-year-9', savingsAverageDeath9);
        this.updateElement('average-savings-death-year-10', savingsAverageDeath10);

        this.updateElement('average-savings-turnover-year-0', savingsAverageTurnover0);
        this.updateElement('average-savings-turnover-year-1', savingsAverageTurnover1);
        this.updateElement('average-savings-turnover-year-2', savingsAverageTurnover2);
        this.updateElement('average-savings-turnover-year-3', savingsAverageTurnover3);
        this.updateElement('average-savings-turnover-year-4', savingsAverageTurnover4);
        this.updateElement('average-savings-turnover-year-5', savingsAverageTurnover5);
        this.updateElement('average-savings-turnover-year-6', savingsAverageTurnover6);
        this.updateElement('average-savings-turnover-year-7', savingsAverageTurnover7);
        this.updateElement('average-savings-turnover-year-8', savingsAverageTurnover8);
        this.updateElement('average-savings-turnover-year-9', savingsAverageTurnover9);
        this.updateElement('average-savings-turnover-year-10', savingsAverageTurnover10);

        this.updateElement('average-savings-productivity-year-0', savingsAverageProductivity0);
        this.updateElement('average-savings-productivity-year-1', savingsAverageProductivity1);
        this.updateElement('average-savings-productivity-year-2', savingsAverageProductivity2);
        this.updateElement('average-savings-productivity-year-3', savingsAverageProductivity3);
        this.updateElement('average-savings-productivity-year-4', savingsAverageProductivity4);
        this.updateElement('average-savings-productivity-year-5', savingsAverageProductivity5);
        this.updateElement('average-savings-productivity-year-6', savingsAverageProductivity6);
        this.updateElement('average-savings-productivity-year-7', savingsAverageProductivity7);
        this.updateElement('average-savings-productivity-year-8', savingsAverageProductivity8);
        this.updateElement('average-savings-productivity-year-9', savingsAverageProductivity9);
        this.updateElement('average-savings-productivity-year-10', savingsAverageProductivity10);

        this.updateTotal('average-total-year-0', this.averageSavings0);
        this.updateTotal('average-total-year-1', this.averageSavings1);
        this.updateTotal('average-total-year-2', this.averageSavings2);
        this.updateTotal('average-total-year-3', this.averageSavings3);
        this.updateTotal('average-total-year-4', this.averageSavings4);
        this.updateTotal('average-total-year-5', this.averageSavings5);
        this.updateTotal('average-total-year-6', this.averageSavings6);
        this.updateTotal('average-total-year-7', this.averageSavings7);
        this.updateTotal('average-total-year-8', this.averageSavings8);
        this.updateTotal('average-total-year-9', this.averageSavings9);
        this.updateTotal('average-total-year-10', this.averageSavings10);

        this.updateElement('npv-absence-year-0', npvAbsence0);
        this.updateElement('npv-absence-year-1', npvAbsence1);
        this.updateElement('npv-absence-year-2', npvAbsence2);
        this.updateElement('npv-absence-year-3', npvAbsence3);
        this.updateElement('npv-absence-year-4', npvAbsence4);
        this.updateElement('npv-absence-year-5', npvAbsence5);
        this.updateElement('npv-absence-year-6', npvAbsence6);
        this.updateElement('npv-absence-year-7', npvAbsence7);
        this.updateElement('npv-absence-year-8', npvAbsence8);
        this.updateElement('npv-absence-year-9', npvAbsence9);
        this.updateElement('npv-absence-year-10', npvAbsence10);

        this.updateElement('npv-presence-year-0', npvPresence0);
        this.updateElement('npv-presence-year-1', npvPresence1);
        this.updateElement('npv-presence-year-2', npvPresence2);
        this.updateElement('npv-presence-year-3', npvPresence3);
        this.updateElement('npv-presence-year-4', npvPresence4);
        this.updateElement('npv-presence-year-5', npvPresence5);
        this.updateElement('npv-presence-year-6', npvPresence6);
        this.updateElement('npv-presence-year-7', npvPresence7);
        this.updateElement('npv-presence-year-8', npvPresence8);
        this.updateElement('npv-presence-year-9', npvPresence9);
        this.updateElement('npv-presence-year-10', npvPresence10);

        this.updateElement('npv-medical-year-0', npvMedical0);
        this.updateElement('npv-medical-year-1', npvMedical1);
        this.updateElement('npv-medical-year-2', npvMedical2);
        this.updateElement('npv-medical-year-3', npvMedical3);
        this.updateElement('npv-medical-year-4', npvMedical4);
        this.updateElement('npv-medical-year-5', npvMedical5);
        this.updateElement('npv-medical-year-6', npvMedical6);
        this.updateElement('npv-medical-year-7', npvMedical7);
        this.updateElement('npv-medical-year-8', npvMedical8);
        this.updateElement('npv-medical-year-9', npvMedical9);
        this.updateElement('npv-medical-year-10', npvMedical10);

        this.updateElement('npv-disable-year-0', npvDisable0);
        this.updateElement('npv-disable-year-1', npvDisable1);
        this.updateElement('npv-disable-year-2', npvDisable2);
        this.updateElement('npv-disable-year-3', npvDisable3);
        this.updateElement('npv-disable-year-4', npvDisable4);
        this.updateElement('npv-disable-year-5', npvDisable5);
        this.updateElement('npv-disable-year-6', npvDisable6);
        this.updateElement('npv-disable-year-7', npvDisable7);
        this.updateElement('npv-disable-year-8', npvDisable8);
        this.updateElement('npv-disable-year-9', npvDisable9);
        this.updateElement('npv-disable-year-10', npvDisable10);

        this.updateElement('npv-death-year-0', npvDeath0);
        this.updateElement('npv-death-year-1', npvDeath1);
        this.updateElement('npv-death-year-2', npvDeath2);
        this.updateElement('npv-death-year-3', npvDeath3);
        this.updateElement('npv-death-year-4', npvDeath4);
        this.updateElement('npv-death-year-5', npvDeath5);
        this.updateElement('npv-death-year-6', npvDeath6);
        this.updateElement('npv-death-year-7', npvDeath7);
        this.updateElement('npv-death-year-8', npvDeath8);
        this.updateElement('npv-death-year-9', npvDeath9);
        this.updateElement('npv-death-year-10', npvDeath10);

        this.updateElement('npv-turnover-year-0', npvTurnover0);
        this.updateElement('npv-turnover-year-1', npvTurnover1);
        this.updateElement('npv-turnover-year-2', npvTurnover2);
        this.updateElement('npv-turnover-year-3', npvTurnover3);
        this.updateElement('npv-turnover-year-4', npvTurnover4);
        this.updateElement('npv-turnover-year-5', npvTurnover5);
        this.updateElement('npv-turnover-year-6', npvTurnover6);
        this.updateElement('npv-turnover-year-7', npvTurnover7);
        this.updateElement('npv-turnover-year-8', npvTurnover8);
        this.updateElement('npv-turnover-year-9', npvTurnover9);
        this.updateElement('npv-turnover-year-10', npvTurnover10);

        this.updateElement('npv-productivity-year-0', npvProductivity0);
        this.updateElement('npv-productivity-year-1', npvProductivity1);
        this.updateElement('npv-productivity-year-2', npvProductivity2);
        this.updateElement('npv-productivity-year-3', npvProductivity3);
        this.updateElement('npv-productivity-year-4', npvProductivity4);
        this.updateElement('npv-productivity-year-5', npvProductivity5);
        this.updateElement('npv-productivity-year-6', npvProductivity6);
        this.updateElement('npv-productivity-year-7', npvProductivity7);
        this.updateElement('npv-productivity-year-8', npvProductivity8);
        this.updateElement('npv-productivity-year-9', npvProductivity9);
        this.updateElement('npv-productivity-year-10', npvProductivity10);

        this.updateTotal('npv-total-year-0', this.npvSavings0);
        this.updateTotal('npv-total-year-1', this.npvSavings1);
        this.updateTotal('npv-total-year-2', this.npvSavings2);
        this.updateTotal('npv-total-year-3', this.npvSavings3);
        this.updateTotal('npv-total-year-4', this.npvSavings4);
        this.updateTotal('npv-total-year-5', this.npvSavings5);
        this.updateTotal('npv-total-year-6', this.npvSavings6);
        this.updateTotal('npv-total-year-7', this.npvSavings7);
        this.updateTotal('npv-total-year-8', this.npvSavings8);
        this.updateTotal('npv-total-year-9', this.npvSavings9);
        this.updateTotal('npv-total-year-10', this.npvSavings10);

        this.updateElement('npv-costs-year-0', npvCosts0);
        this.updateElement('npv-costs-year-1', npvCosts1);
        this.updateElement('npv-costs-year-2', npvCosts2);
        this.updateElement('npv-costs-year-3', npvCosts3);
        this.updateElement('npv-costs-year-4', npvCosts4);
        this.updateElement('npv-costs-year-5', npvCosts5);
        this.updateElement('npv-costs-year-6', npvCosts6);
        this.updateElement('npv-costs-year-7', npvCosts7);
        this.updateElement('npv-costs-year-8', npvCosts8);
        this.updateElement('npv-costs-year-9', npvCosts9);
        this.updateElement('npv-costs-year-10', npvCosts10);

        let npvNetSavings0 = this.npvSavings0 - npvCosts0;
        let npvNetSavings1 = this.npvSavings1 - npvCosts1;
        let npvNetSavings2 = this.npvSavings2 - npvCosts2;
        let npvNetSavings3 = this.npvSavings3 - npvCosts3;
        let npvNetSavings4 = this.npvSavings4 - npvCosts4;
        let npvNetSavings5 = this.npvSavings5 - npvCosts5;
        let npvNetSavings6 = this.npvSavings6 - npvCosts6;
        let npvNetSavings7 = this.npvSavings7 - npvCosts7;
        let npvNetSavings8 = this.npvSavings8 - npvCosts8;
        let npvNetSavings9 = this.npvSavings9 - npvCosts9;
        let npvNetSavings10 = this.npvSavings10 - npvCosts10;

        this.updateElement('npv-savings-year-0', npvNetSavings0);
        this.updateElement('npv-savings-year-1', npvNetSavings1);
        this.updateElement('npv-savings-year-2', npvNetSavings2);
        this.updateElement('npv-savings-year-3', npvNetSavings3);
        this.updateElement('npv-savings-year-4', npvNetSavings4);
        this.updateElement('npv-savings-year-5', npvNetSavings5);
        this.updateElement('npv-savings-year-6', npvNetSavings6);
        this.updateElement('npv-savings-year-7', npvNetSavings7);
        this.updateElement('npv-savings-year-8', npvNetSavings8);
        this.updateElement('npv-savings-year-9', npvNetSavings9);
        this.updateElement('npv-savings-year-10', npvNetSavings10);

        let totalAbsence = this.getNPV(absenceCashFlow);
        let totalPresence = this.getNPV(presenceCashFlow);
        let totalMedical = this.getNPV(medicalCashFlow);
        let totalDisable = this.getNPV(disableCashFlow);
        let totalDeath = this.getNPV(deathCashFlow);
        let totalTurnover = this.getNPV(turnoverCashFlow);
        let totalProductivity = this.getNPV(productivityCashFlow);

        let totalSavings = totalAbsence + totalPresence + totalMedical + totalDisable + totalDeath + totalTurnover + totalProductivity;
        let totalInterventionCost = npvCosts0 + npvCosts1 + npvCosts2 + npvCosts3 + npvCosts4 + npvCosts5 + npvCosts6 + npvCosts7 + npvCosts8 + npvCosts9 + npvCosts10;

        this.updateElement('total-absence', totalAbsence);
        this.updateElement('per-employee-absence', totalAbsence / this.employeeNumber);
        this.updateElement('total-presence', totalPresence);
        this.updateElement('per-employee-presence', totalPresence / this.employeeNumber);
        this.updateElement('total-medical-benefit', totalMedical);
        this.updateElement('per-employee-medical-benefit', totalMedical / this.employeeNumber);
        this.updateElement('total-disable-benefit', totalDisable);
        this.updateElement('per-employee-disable-benefit', totalDisable / this.employeeNumber);
        this.updateElement('total-death-benefit', totalDeath);
        this.updateElement('per-employee-death-benefit', totalDeath / this.employeeNumber);
        this.updateElement('total-turnover', totalTurnover);
        this.updateElement('per-employee-turnover', totalTurnover / this.employeeNumber);
        this.updateElement('total-productivity', totalProductivity);
        this.updateElement('per-employee-productivity', totalProductivity / this.employeeNumber);
        this.updateElement('total-savings', totalSavings);
        this.updateElement('per-employee-savings', totalSavings / this.employeeNumber);
        this.updateElement('total-intervention-cost', totalInterventionCost);
        this.updateElement('per-employee-intervention-cost', totalInterventionCost / this.employeeNumber);

        this.updateElement('total-savings-wellness', totalSavings);
        this.updateElement('total-cost-wellness', totalInterventionCost);
        this.updateElement('net-savings-wellness', totalSavings - totalInterventionCost);
        this.updateElement('investment-ratio', totalSavings / totalInterventionCost );
        document.getElementById('investment-ratio').innerText = (totalSavings / totalInterventionCost).toFixed(2);
    }


    addCurrency(amount): string{
        return Math.round(amount).toLocaleString('en-GB', { style: 'currency', currency: 'GBP'})
    }

    updateElement(element: string, value: number): void {
        if(element.indexOf('average-savings') > -1){
            if(element.indexOf('-0') > -1){
                this.averageSavings0 += value;
            }
            if(element.indexOf('-1') > -1 && element.indexOf('-10') === -1){
                this.averageSavings1 += value;
            }
            if(element.indexOf('-2') > -1){
                this.averageSavings2 += value;
            }
            if(element.indexOf('-3') > -1){
                this.averageSavings3 += value;
            }
            if(element.indexOf('-4') > -1){
                this.averageSavings4 += value;
            }
            if(element.indexOf('-5') > -1){
                this.averageSavings5 += value;
            }
            if(element.indexOf('-6') > -1){
                this.averageSavings6 += value;
            }
            if(element.indexOf('-7') > -1){
                this.averageSavings7 += value;
            }
            if(element.indexOf('-8') > -1){
                this.averageSavings8 += value;
            }
            if(element.indexOf('-9') > -1){
                this.averageSavings9 += value;
            }
            if(element.indexOf('-10') > -1){
                this.averageSavings10 += value;
            }
        }
        if(element.indexOf('npv') > -1 && element.indexOf('savings') === -1 && element.indexOf('costs') === -1 && element.indexOf('total') === -1) {
            if(element.indexOf('-0') > -1){
                this.npvSavings0 += value;
            }
            if(element.indexOf('-1') > -1 && element.indexOf('-10') === -1){
                this.npvSavings1 += value;
            }
            if(element.indexOf('-2') > -1){
                this.npvSavings2 += value;
            }
            if(element.indexOf('-3') > -1){
                this.npvSavings3 += value;
            }
            if(element.indexOf('-4') > -1){
                this.npvSavings4 += value;
            }
            if(element.indexOf('-5') > -1){
                this.npvSavings5 += value;
            }
            if(element.indexOf('-6') > -1){
                this.npvSavings6 += value;
            }
            if(element.indexOf('-7') > -1){
                this.npvSavings7 += value;
            }
            if(element.indexOf('-8') > -1){
                this.npvSavings8 += value;
            }
            if(element.indexOf('-9') > -1){
                this.npvSavings9 += value;
            }
            if(element.indexOf('-10') > -1){
                this.npvSavings10 += value;
            }
        }
        document.getElementById(element).innerText = this.addCurrency(value);
    }

    updateTotal(element, value): void{
        document.getElementById(element).innerText = this.addCurrency(value);
    }

    savingAnnual (value: number, inflation: number, power: number): number {
        let finalValue;
        if(power === 0) {
            this.total0 += value;
            return value;
        }
        finalValue = value * this.residualImpact * Math.pow(1 + inflation, power);
        if(power === 1) {
            this.total1 += finalValue;
        }
        if(power === 2) {
            this.total2 += finalValue;
        }
        if(power === 3) {
            this.total3 += finalValue;
        }
        if(power === 4) {
            this.total4 += finalValue;
        }
        if(power === 5) {
            this.total5 += finalValue;
        }
        if(power === 6) {
            this.total6 += finalValue;
        }
        if(power === 7) {
            this.total7 += finalValue;
        }
        if(power === 8) {
            this.total8 += finalValue;
        }
        if(power === 9) {
            this.total9 += finalValue;
        }
        if(power === 10) {
            this.total10 += finalValue;
        }
        return finalValue;
    }

    savingAverage(value): number {
        return value / this.employeeNumber;
    }

    calculateNPVSavings(value, coefficient): number {
        return value * Math.pow(1 + this.IRRforROI, -coefficient);
    }

    calculateNPVCosts(coefficient): number {
        return (this.employeeNumber * this.renewalServicesCost) / (Math.pow(1 + this.IRRforROI, coefficient));
    }

    getNPV(cashFlows): number {
        let npv = 0;
        for (let i = 0; i < cashFlows.length; i++) {
            npv += cashFlows[i] / Math.pow(this.IRRforROI + 1, i + 1);
        }
        return npv * Math.pow(1 + this.IRRforROI, 0.5);
    }

    confirm() {
        this.employeeNumber = Number(this.employeesInput.value);
        this.avgPayroll = Number(this.payrollInput.value);
        this.interventionCost = Number(this.interventionCostInput.value);
        this.renewalServicesCost = Number(this.maintenanceCostInput.value);
        this.includeProductivityInput.checked ? this.includeProd = 'Yes' : this.includeProd = 'No';
        this.calculate();
        this.employeesInput.value = "";
        this.payrollInput.value = "";
        this.interventionCostInput.value="";
        this.maintenanceCostInput.value = "";
        this.includeProductivityInput.checked = false;
    }

    myFunction() {
        if(this.employeesInput.value && this.interventionCostInput.value && this.maintenanceCostInput.value) {
            this.btn.disabled = false;
        }
    }
}

const analysis = new ROIAnalysis();
